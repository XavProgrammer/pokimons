/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Batalla.Batalla;
import static GUIs.Mundo.imagenJugador;
import static GUIs.Mundo.j;
import Main.Main;
import Pokemon.Lider;
import Pokemon.MundoPokemon;
import Pokemon.Pokemon;
import java.io.File;
import java.util.ArrayList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import static javafx.scene.paint.Color.ALICEBLUE;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.BLUE;
import static javafx.scene.paint.Color.TRANSPARENT;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Byron
 */
public class Gym {
    public static int i = 0;
    public static StackPane root = new StackPane();
    public static ImageView imgGym = new ImageView(new Image("/ImgEscenario/fondo gimnasio.png"));
    public static ImageView imgLider= new ImageView(new Image("/ImgEntrenador/LiderMini.png"));
    public static ImageView imgLider2 = new ImageView(new Image("/ImgEscenario/LiderMiniGym.png"));
    public static Button btnInteraccion = new Button("Salir.");
    public static Pane paneGym = new Pane();
    //private ImageView personaje = new ImageView("/ImgEntrenador/M adelante1.png");
    public static ImageView estatuas = new ImageView("/ImgEscenario/Estatuas.png");
    public static Rectangle r1 = new Rectangle(12,3);
    public static Rectangle r2 = new Rectangle(12,3);
    public static Rectangle rArriba = new Rectangle(145,10);
    public static Rectangle rAbajo = new Rectangle(145,10);
    public static Rectangle rIzquierda = new Rectangle(10,102);
    public static Rectangle rDerecha = new Rectangle(10,102);
    public static Rectangle rLider = new Rectangle(14,3);
    public static Rectangle rSalida = new Rectangle(26,14);
    public static MediaPlayer mediaplayer = new MediaPlayer(new Media(new File("src/musica/Gimnasio.mp3").toURI().toString()));
    
    
    public static Pokemon poke5 = MundoPokemon.getPokemonesDisponibles().get(143);
    public static    Pokemon poke6 = MundoPokemon.getPokemonesDisponibles().get(144);
    public static    Pokemon poke7 = MundoPokemon.getPokemonesDisponibles().get(145);
    public static    Pokemon poke8 = MundoPokemon.getPokemonesDisponibles().get(149);
    public static    Pokemon poke9 = MundoPokemon.getPokemonesDisponibles().get(150);
        
    public static ArrayList<Pokemon> pokeLider = new ArrayList<>();
    
    public static Lider lider = new Lider("Líder Angela","Femenino",pokeLider);
    
    public static Button inicioBatalla = new Button("Lider");
    
    public Gym(){
        
        pokeLider.add(poke5);
        pokeLider.add(poke6);
        pokeLider.add(poke7);
        pokeLider.add(poke8);
        pokeLider.add(poke9);
        
        
        
      
    }
    public static void organizarElementos(){
        root.getChildren().addAll(paneGym);
        paneGym.getChildren().addAll(imgGym,rArriba,rAbajo,rIzquierda,rDerecha,imgLider,imagenJugador,imgLider2,rLider,rSalida,estatuas,r1,r2);
        
        
        
        imgGym.setLayoutY(134);
        imgGym.setLayoutX(212);
        
        imagenJugador.setOnKeyPressed(e -> j.movimientoJugador(imagenJugador, j, e));
        
        imagenJugador.setFitHeight(19);
        imagenJugador.setFitWidth(16);
        imagenJugador.setLayoutX(292);
        imagenJugador.setLayoutY(232);
        rSalida.setLayoutX(287);
        rSalida.setLayoutY(262);
        imgLider.setLayoutX(293);
        imgLider.setLayoutY(197);
        imgLider2.setLayoutX(293);
        imgLider2.setLayoutY(197);
        estatuas.setLayoutX(260);
        estatuas.setLayoutY(187);
        r1.setLayoutX(262);
        r1.setLayoutY(203);
        r2.setLayoutX(325);
        r2.setLayoutY(203);
        //r1.setFill(TRANSPARENT);
        //r2.setFill(TRANSPARENT);
        
        rArriba.setLayoutX(228);
        rArriba.setLayoutY(155);
        rAbajo.setLayoutX(228);
        rAbajo.setLayoutY(267);
        rIzquierda.setLayoutX(218);
        rIzquierda.setLayoutY(165);
        rDerecha.setLayoutX(373);
        rDerecha.setLayoutY(165);
        rArriba.setFill(TRANSPARENT);
        rAbajo.setFill(TRANSPARENT);
        rIzquierda.setFill(TRANSPARENT);
        rDerecha.setFill(TRANSPARENT);
        
        rLider.setLayoutX(293);
        rLider.setLayoutY(203);
        rLider.setFill(BLACK);
        btnInteraccion.setLayoutX(450);
        btnInteraccion.setLayoutY(250);

        root.getChildren().add(inicioBatalla);
        inicioBatalla.setOnAction(e ->batalla());
        
        
    }
    
    public static void batalla(){
        salirEscena();
                Batalla battle1 = new Batalla(j,lider);
                Scene battleScene = new Scene(battle1.getRoot(),600,400);
                Main.window.setScene(battleScene);
    }
    public static void crearEscena(){
        i=i+2;
        organizarElementos();
        Main.window.setScene(Main.sGym);
        imagenJugador.setLayoutY(imagenJugador.getLayoutY()-i);
        System.out.println(i);
        mediaplayer.play();
        
    }
    public static void salirEscena(){
        root.getChildren().clear();
        paneGym.getChildren().clear();
        Mundo.crearEscena();
        
        Main.window.setScene(Main.sMun);
        Mundo.imagenJugador.setLayoutX(336.5);
        Mundo.imagenJugador.setLayoutY(325.0);
        mediaplayer.stop();
        
    }
    public static StackPane getRoot(){
        return root;
    }
}
