/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Main.Main;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Font;

/**
 *
 * @author Andres
 */
public class Inicio {
    ImageView fondo = new ImageView(new Image("/images/PIKAPIKA.jpg"));
    Label lbl = new Label("BIENVENIDO A POKéMON");
    Button btnJuegoNuevo = new Button("Juego Nuevo");
    Button btnCargar = new Button("Cargar Juego");
    Button btnSalir = new Button("Salir");
    StackPane root = new StackPane();
    VBox paneBtns = new VBox(10);
    public static MediaPlayer mediaplayer = new MediaPlayer(new Media(new File("src/musica/Registro.mp3").toURI().toString()));
    
    /**
     * Constructor de la escena Inicio
     */
    public Inicio(){
        mediaplayer.play();
        organizarElementos();
       
        
    
        
    }
    /**
     * Organiza los elementos de la escena
     */
    public void organizarElementos(){
        File archivo = new File("jugador.ser");
        File archivo1 = new File("mundoPokemon.ser");
        btnCargar.setDisable(true);
        if (archivo.exists()||archivo1.exists()){
            btnJuegoNuevo.setDisable(true);
            btnCargar.setDisable(false);
            
        }
        lbl.setFont(Font.font("Minecraftia", 15));
        btnJuegoNuevo.setFont(Font.font("Minecraftia", 15));
        btnCargar.setFont(Font.font("Minecraftia", 15));
        btnSalir.setFont(Font.font("Minecraftia", 15));
        fondo.setFitHeight(400);
        fondo.setFitWidth(600);
        fondo.setOpacity(0.5);
        paneBtns.setAlignment(Pos.CENTER);
        
        paneBtns.getChildren().addAll(lbl,btnJuegoNuevo, btnCargar, btnSalir);
        root.getChildren().addAll(fondo, paneBtns);
        
        

    }
    /**
     * Getter, retorna al boton 
     * @return btnsJuegoNuevo
     */
    public Button getButton(){
        return this.btnJuegoNuevo;
    }
    /**
     * Retorna el root de la escena
     * @return root
     */    
    public StackPane getRoot(){
        return this.root;
    }   
    /**
     * Retorna el boton de cargar
     * @return btnCargar
     */

    public Button getButtonCarg() {
        return this.btnCargar;
    }
    /**
     * Sale del programa
     * @return btnSalir
     */
    public Button getButtonSalir() {
        return this.btnSalir;
    }
    
}
