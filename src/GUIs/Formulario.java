/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

/**
 *
 * @author Andres
 */
public class Formulario {

    String genero;
    Label lbl = new Label("Nombre: ");
    ImageView fondo = new ImageView(new Image("/images/PIKAPIKA.jpg"));
    Button btnContinuar = new Button("Continuar");
    Label lblGenero = new Label("Genero: ");
    Button btnMasculino = new Button();
    Button btnFemenino = new Button();

    ImageView imgMasc = new ImageView(new Image("/images/masculino.png"));
    ImageView imgFeme = new ImageView(new Image("/images/femenino.png"));

    TextField txtNombre = new TextField();
    HBox paneBtns = new HBox(20);
    VBox pane = new VBox(20);
    StackPane root = new StackPane();

    /**
     * Constructor de Formulario
     */
    public Formulario() {

        organizarElementos();

        eventos();
    }

    /**
     * Organiza los elementos de la escena
     */
    public void organizarElementos() {
        lbl.setFont(Font.font("Minecraftia", 15));

        btnContinuar.setFont(Font.font("Minecraftia", 15));
        lblGenero.setFont(Font.font("Minecraftia", 15));
        btnMasculino.setFont(Font.font("Minecraftia", 15));
        btnFemenino.setFont(Font.font("Minecraftia", 15));

        btnMasculino.setGraphic(imgMasc);
        btnFemenino.setGraphic(imgFeme);

        fondo.setFitHeight(400);
        fondo.setFitWidth(600);
        fondo.setOpacity(0.5);
        btnContinuar.setDisable(true);
        paneBtns.getChildren().addAll(btnFemenino, btnMasculino);
        paneBtns.setAlignment(Pos.CENTER);
        txtNombre.setAlignment(Pos.CENTER);
        txtNombre.setMaxWidth(200);
        pane.setAlignment(Pos.CENTER);
        pane.getChildren().addAll(lbl, txtNombre, lblGenero, paneBtns, btnContinuar);
        pane.setAlignment(Pos.CENTER);
        btnContinuar.setAlignment(Pos.CENTER);

        root.getChildren().addAll(fondo, pane);
    }

    /**
     * Genera los eventos de los nodos de la escena
     */
    public void eventos() {
        txtNombre.setOnKeyReleased(e -> activarBtn());
        txtNombre.setOnKeyTyped(e -> limitar(txtNombre.getText(), e));
        btnMasculino.setOnAction(e -> bloquearBtn(btnMasculino));
        btnFemenino.setOnAction(e -> bloquearBtn(btnFemenino));
        btnMasculino.setOnMouseEntered(e -> btnMasculino.setEffect(new InnerShadow()));
        btnFemenino.setOnMouseEntered(e -> btnFemenino.setEffect(new InnerShadow()));
        btnMasculino.setOnMouseExited(e -> btnMasculino.setEffect(null));
        btnFemenino.setOnMouseExited(e -> btnFemenino.setEffect(null));

    }

    /**
     *
     * @return root
     */

    public StackPane getRoot() {
        return root;
    }

    /**
     *
     * @return btnContinuar
     */
    public Button getButton() {

        return this.btnContinuar;
    }

    /**
     * Bloquea el boton que no se pulsa
     *
     * @param btn
     */
    public void bloquearBtn(Button btn) {
        if (btn.equals(this.btnMasculino)) {
            genero = "Masculino";
            btnFemenino.setDisable(true);
            activarBtn();

        } else {
            btnMasculino.setDisable(true);
            genero = "Femenino";
            activarBtn();
        }
    }

    /**
     * Activa el boton btnContinuar
     */
    public void activarBtn() {
        boolean isDisabl = ((txtNombre.getText().equals("") | this.genero == null));
        btnContinuar.setDisable(isDisabl);
    }

    /**
     * getter del nombre
     *
     * @return txtNombre.getText
     */
    public String getNombre() {
        return txtNombre.getText();
    }

    /**
     * retorna el genero ingresado
     *
     * @return genero
     */
    public String getGenero() {
        return this.genero;
    }

    public void limitar(String texto, Event ev) {

        if (texto.length() == 12) {
            ev.consume();
        }

    }

}
