/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Pokemon.Jugador;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author Andres
 */
public class Seleccion {

    Jugador j;
    String nombre, genero;
    ImageView fondo = new ImageView(new Image("/images/PIKAPIKA.jpg"));
    ImageView imgBulb = new ImageView(new Image("/images/bulbasaur.png"));
    ImageView imgCharm = new ImageView(new Image("/images/charmander.png"));
    ImageView imgSqui = new ImageView(new Image("/images/squirtle.png"));

    Label lblDescripcion = new Label("Bienvenido al Mundo Pokemon. Para iniciar la aventura escoje un pokemon: ");
    Button btnBulbasaur = new Button();
    Button btnCharmander = new Button();
    Button btnSquirtle = new Button();
    Button btnIniciarJuego = new Button("Iniciar Juego");
    HBox paneBtns = new HBox(20);
    VBox pane = new VBox(10);
    StackPane root = new StackPane();

    /**
     * Constructor de la escena Seleccion
     */
    public Seleccion() {
        organizarElementos();
        eventos();

    }

    /**
     * organiza los elementos de la escena
     */
    public void organizarElementos() {

        lblDescripcion.setFont(Font.font("Minecraftia", 15));
        btnBulbasaur.setFont(Font.font("Minecraftia", 15));
        btnCharmander.setFont(Font.font("Minecraftia", 15));
        btnSquirtle.setFont(Font.font("Minecraftia", 15));
        btnIniciarJuego.setFont(Font.font("Minecraftia", 15));
        btnBulbasaur.setGraphic(imgBulb);
        btnCharmander.setGraphic(imgCharm);
        btnSquirtle.setGraphic(imgSqui);
        fondo.setFitHeight(400);
        fondo.setFitWidth(600);
        fondo.setOpacity(0.5);
        btnIniciarJuego.setDisable(true);
        btnIniciarJuego.setAlignment(Pos.CENTER);
        paneBtns.getChildren().addAll(btnBulbasaur, btnCharmander, btnSquirtle);
        paneBtns.setAlignment(Pos.CENTER);

        pane.getChildren().addAll(lblDescripcion, paneBtns, btnIniciarJuego);
        pane.setAlignment(Pos.CENTER);
        root.getChildren().addAll(fondo, pane);
    }

    /**
     * retorna el root principal
     *
     * @return root
     */

    public StackPane getRoot() {
        return root;
    }

    /**
     * retorna el boton de inicio de juego
     *
     * @return btnIniciarJuego
     */
    public Button getButton() {
        return this.btnIniciarJuego;
    }

    /**
     * Bloquea los botones que no se pulsan
     *
     * @param btn
     */
    public void bloquearBtn(Button btn) {
        if (btn.equals(btnBulbasaur)) {
            btnCharmander.setDisable(false);
            btnSquirtle.setDisable(false);
            btnIniciarJuego.setDisable(false);
        } else if (btn.equals(btnCharmander)) {
            btnBulbasaur.setDisable(true);
            btnSquirtle.setDisable(true);
            btnIniciarJuego.setDisable(false);
        } else {
            btnBulbasaur.setDisable(true);
            btnCharmander.setDisable(true);
            btnIniciarJuego.setDisable(false);
        }
    }

    /**
     * genera los eventos de la escena
     */
    public void eventos() {
        btnCharmander.setOnMouseClicked(e -> bloquearBtn(btnCharmander));
        btnBulbasaur.setOnMouseClicked(e -> bloquearBtn(btnBulbasaur));
        btnSquirtle.setOnMouseClicked(e -> bloquearBtn(btnSquirtle));
        btnCharmander.setOnMouseEntered(e -> btnCharmander.setEffect(new InnerShadow()));
        btnBulbasaur.setOnMouseEntered(e -> btnBulbasaur.setEffect(new InnerShadow()));
        btnSquirtle.setOnMouseEntered(e -> btnSquirtle.setEffect(new InnerShadow()));
        btnCharmander.setOnMouseExited(e -> btnCharmander.setEffect(null));
        btnBulbasaur.setOnMouseExited(e -> btnBulbasaur.setEffect(null));
        btnSquirtle.setOnMouseExited(e -> btnSquirtle.setEffect(null));
    }

    /**
     * set el atributo nombre
     *
     * @param n
     */
    public void setNombre(String n) {
        this.nombre = n;
    }

    /**
     * set el atributo genero
     *
     * @param g
     */
    public void setGenero(String g) {
        this.genero = g;
    }

    /**
     * Devuelve el boton de bulbasaur
     *
     * @return btnBulbasaur
     */
    public Button getBtnBulbasaur() {
        return btnBulbasaur;
    }

    /**
     * Devuelve el boton de Charmander
     *
     * @return btnCharmander
     */
    public Button getBtnCharmander() {
        return btnCharmander;
    }

    /**
     * devuelve el boton de Squirtle
     *
     * @return btnSquirtle
     */
    public Button getBtnSquirtle() {
        return btnSquirtle;
    }

}
