/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Main.Main;
import static Main.Main.sMun;
import Pokemon.Cesped;
import Pokemon.Dominguero;
import Pokemon.Jugador;
import Pokemon.MundoPokemon;
import Pokemon.Pokemon;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.TRANSPARENT;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author Angie
 */
public class Mundo {

    public static MediaPlayer mediaplayer = new MediaPlayer(new Media(new File("src/musica/Ciudad.mp3").toURI().toString()));
    public static ArrayList<Cesped> cespedPoke = new ArrayList<>();
    public static ArrayList<Rectangle> rArbol = new ArrayList<>();
    public static Pane bordesTop = new Pane();
    public static StackPane root = new StackPane();
    public static Pane piso = new Pane();
    public static Jugador j = crearJugador("jugador.ser"); //Se deserializa el jugador y se crea la base
    public static ImageView imagenJugador = new ImageView(new Image(j.getImagePathAdel1()));
    public static ImageView bgPiso = new ImageView(new Image("/ImgEscenario/BGPisoRefinado3.0.png"));
    Button btnCerrar = new Button("Cerrar");
    //OBJETOS BORDES DE MUNDO
    public static Rectangle rBordeTop = new Rectangle(600, 50);
    public static Rectangle rBordeLeft = new Rectangle(50, 400);
    public static Rectangle rBordeRight = new Rectangle(50, 400);
    public static Rectangle rBordeBot = new Rectangle(600, 50);
    public static Rectangle rHosp = new Rectangle(150, 125);
    public static Rectangle rCasa = new Rectangle(50, 75);
    public static Rectangle rGym = new Rectangle(100, 75);
    public static Rectangle rLake = new Rectangle(100, 45);
    public static Rectangle rPuertaGym = new Rectangle(30, 10);
    public static Rectangle rPuertaHosp = new Rectangle(20, 10);
    //Arbol
    public static Rectangle a1 = new Rectangle(50, 40);
    public static Rectangle a2 = new Rectangle(50, 100);
    public static Rectangle a3 = new Rectangle(100, 50);
    public static Rectangle a4 = new Rectangle(50, 100);
    //CESPED
    public static Cesped c1 = new Cesped();
    public static Cesped c2 = new Cesped();
    public static Cesped c3 = new Cesped();

    public static Thread t1; 
    
    public static Random r = new Random();

    public Mundo() {
        
    }

    public static void organizarElementos() {

        piso.getChildren().addAll(rBordeTop, rBordeLeft, rBordeRight, rBordeBot, rHosp, rCasa, rGym, rLake, rPuertaHosp, rPuertaGym, bgPiso, imagenJugador);
        rBordeTop.setLayoutX(0);
        rBordeTop.setLayoutY(0);
        rBordeTop.setFill(Color.BLACK);
        rBordeLeft.setLayoutX(0);
        rBordeLeft.setLayoutY(0);
        rBordeLeft.setFill(Color.BLACK);
        rBordeRight.setLayoutX(550);
        rBordeTop.setLayoutY(0);
        rBordeTop.setFill(Color.BLACK);
        rBordeBot.setLayoutX(0);
        rBordeBot.setLayoutY(350);
        rBordeBot.setFill(Color.BLACK);
        rHosp.setLayoutX(50);
        rHosp.setLayoutY(50);
        rHosp.setFill(Color.BLACK);
        rCasa.setLayoutX(500);
        rCasa.setLayoutY(50);
        rCasa.setFill(Color.BLACK);
        rGym.setLayoutX(300);
        rGym.setLayoutY(225);
        rGym.setFill(Color.BLACK);
        rLake.setLayoutX(300);
        rLake.setLayoutY(100);
        rLake.setFill(Color.BLACK);
        rPuertaHosp.setLayoutX(112.5);
        rPuertaHosp.setLayoutY(175);
        rPuertaHosp.setFill(Color.RED);
        rPuertaGym.setLayoutX(335);
        rPuertaGym.setLayoutY(300);
        rPuertaGym.setFill(Color.RED);

        imagenJugador.setLayoutX(510);
        imagenJugador.setLayoutY(135);
        imagenJugador.setFocusTraversable(true);
        imagenJugador.setFitHeight(25);
        imagenJugador.setFitWidth(25);
        root.getChildren().add(piso);
        imagenJugador.setOnKeyPressed(e -> j.movimientoJugador(imagenJugador, j, e));
        //ARBOLES
        rArbol.add(a1);
        rArbol.add(a2);
        rArbol.add(a3);
        rArbol.add(a4);
        piso.getChildren().addAll(a1, a2, a3, a4);
        a1.setLayoutX(450);
        a1.setLayoutY(80);
        a1.setFill(TRANSPARENT);
        a2.setLayoutX(450);
        a2.setLayoutY(200);
        a2.setFill(TRANSPARENT);
        a3.setLayoutX(200);
        a3.setLayoutY(250);
        a3.setFill(TRANSPARENT);
        a4.setLayoutX(200);
        a4.setLayoutY(50);
        a4.setFill(TRANSPARENT);
        //CESPED        
        cespedPoke.add(c1);
        cespedPoke.add(c2);
        cespedPoke.add(c3);
        piso.getChildren().addAll(c1.getPanelCesped(), c2.getPanelCesped(), c3.getPanelCesped());
        c1.getPanelCesped().setLayoutX(402.5);
        c1.getPanelCesped().setLayoutY(277.5);
        c2.getPanelCesped().setLayoutX(77.5);
        c2.getPanelCesped().setLayoutY(327.5);
        c3.getPanelCesped().setLayoutX(477.5);
        c3.getPanelCesped().setLayoutY(52.5);
        Pokemon poke1 = MundoPokemon.getPokemonesDisponibles().get(10);
        Pokemon poke2 = MundoPokemon.getPokemonesDisponibles().get(55);
        ArrayList<Pokemon> pokeEnemigo = new ArrayList<>();
        pokeEnemigo.add(poke1);
        pokeEnemigo.add(poke2);
        t1= new Thread(new Dominguero("Angie", "Femenino",pokeEnemigo,"/ImgEntrenador/DomingueraMini.png"));
        
    }

    /**
     * define el estado de la thread
     */
    public static void crearEscena() {
        Main.deserializarJugador("jugador.ser");
        root.getChildren().clear();
        piso.getChildren().clear();
        organizarElementos();
        Main.window.setScene(sMun);
        mediaplayer.play();
        if(t1.isAlive()){
            t1.resume();
        }
        else{
            t1.start();
        }
        
    }

    public static void salirEscena() {
        root.getChildren().clear();
        piso.getChildren().clear();
        mediaplayer.stop();
        t1.suspend();
        Main.serializarJugador("jugador.ser", j);
        
    }

    public static StackPane getRoot() {
        return root;
    }

    public void setJ(Jugador j) {
        this.j = j;
    }

    public Jugador getJ() {
        return j;
    }

    public static Rectangle getrBordeTop() {
        return rBordeTop;
    }

    
    
    /**
     * en base a la informacion serializada en formulario, se crea al jugador y se le asignan las imagenes de acuerdo a su genero
     * @param filename
     * @return 
     */
    public static Jugador crearJugador(String filename) {
        Jugador jugador = null;
        ArrayList<Pokemon> pokess = new ArrayList<>();
        try {
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(filename));
            jugador = (Jugador) reader.readObject();
            if (jugador.getGenero().equalsIgnoreCase("Femenino")) {
                jugador.setImagePathAdel1("/ImgEntrenador/F adelante1.png");
                jugador.setImagePathAdel2("/ImgEntrenador/F adelante2.png");
                jugador.setImagePathAtras1("/ImgEntrenador/F atras1.png");
                jugador.setImagePathAtras2("/ImgEntrenador/F atras1.png");
                jugador.setImagePathDer1("/ImgEntrenador/F der1.png");
                jugador.setImagePathDer2("/ImgEntrenador/F der2.png");
                jugador.setImagePathIzq1("/ImgEntrenador/F izq1.png");
                jugador.setImagePathIzq2("/ImgEntrenador/F izq2.png");
            } else {
                jugador.setImagePathAdel1("/ImgEntrenador/M adelante1.png");
                jugador.setImagePathAdel2("/ImgEntrenador/M adelante2.png");
                jugador.setImagePathAtras1("/ImgEntrenador/M atras1.png");
                jugador.setImagePathAtras2("/ImgEntrenador/M atras2.png");
                jugador.setImagePathDer1("/ImgEntrenador/M der1.png");
                jugador.setImagePathDer2("/ImgEntrenador/M der2.png");
                jugador.setImagePathIzq1("/ImgEntrenador/M izq 1.png");
                jugador.setImagePathIzq2("/ImgEntrenador/M izq3.png");
            }
            pokess.add(jugador.getPokeInicial());
            jugador.setPokemonesObtenidos(pokess);

            return jugador;

        } catch (FileNotFoundException ex) {
            System.out.println("NO SE ENCONTRÓ EL JUGADOR: " + ex);
        } catch (IOException ex) {
            System.out.println("ERROR IO: " + ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("NO EXISTE LA CLASE JUGADOR: " + ex);
        }
        return jugador;
    }

    public ArrayList<Cesped> getCespedPoke() {
        return cespedPoke;
    }

    public Pane getPiso() {
        return piso;
    }

    public Button getBtnCerrar() {
        return btnCerrar;
    }

    public ImageView getImagenJugador() {
        return imagenJugador;
    }
    
}
