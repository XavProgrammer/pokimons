/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import static GUIs.Mundo.c1;
import static GUIs.Mundo.c2;
import static GUIs.Mundo.c3;
import static GUIs.Mundo.cespedPoke;
import static GUIs.Mundo.imagenJugador;
import static GUIs.Mundo.j;
import static GUIs.Mundo.piso;
import Main.Main;
import Pokemon.Cesped;
import Pokemon.Jugador;
import Pokemon.Pokemon;
import java.io.File;
import javafx.geometry.Bounds;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import static javafx.scene.paint.Color.AQUAMARINE;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.GREEN;
import static javafx.scene.paint.Color.TRANSPARENT;
import static javafx.scene.paint.Color.WHITE;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Byron
 */
public class Hospital {

    public static Jugador jota = new Jugador();

    public static int k = 0;
    public static StackPane root = new StackPane();
    public static ImageView imgHospital = new ImageView(new Image("/ImgEscenario/fondo hospital.png"));
    public static ImageView imgEnfermera = new ImageView(new Image("/ImgEntrenador/Enfermera.png"));
    public static Pane paneHos = new Pane();
    public static Button btnInteraccion = new Button("Interactuar");

    public static Button btnComprar = new Button("Comprar");
    public static Button btnSanar = new Button("Sanar Pokemons");
    public static Button btnSalir = new Button("Salir");
    public static FlowPane panelOpciones = new FlowPane(Orientation.VERTICAL);

    public static FlowPane panelTienda = new FlowPane(Orientation.VERTICAL);
    public static Button btnPokebolas = new Button("Comprar Pokebola");
    public static Button btnSalirTienda = new Button("Salir");
    public static Label lblTienda = new Label("Tienda");
    public static Label lblDinero = new Label("Dinero: " + Integer.toString(jota.getDinero()));
    public static Label lblSanar = new Label("Tus pokemones fueron sanados, vuelve pronto.");
    public static Button btnOk = new Button("Ok");
    public static VBox panelMensaje = new VBox();

    public static Label lblSinDinero = new Label("No hay dinero suficiente para comprar el item.");
    public static Button btnCSinDinero = new Button("ok");
    public static FlowPane compraRechazada = new FlowPane(Orientation.VERTICAL);

    public static Label lblCompra = new Label("Pokebola comprada con exito.");
    public static Button btnCerrarCompra = new Button("Cerrar Ventana.");
    public static FlowPane compraRealizada = new FlowPane(Orientation.VERTICAL);

    public static Button darDinero = new Button("Dar Dinero");
    //OBSTACULOS
    public static Rectangle rArriba = new Rectangle(240, 10);
    public static Rectangle rIzquierda = new Rectangle(10, 142);
    public static Rectangle rDerecha = new Rectangle(10, 142);
    public static Rectangle rAbajo = new Rectangle(240, 10);
    public static Rectangle rBorde1 = new Rectangle(15, 15);
    public static Rectangle rBorde2 = new Rectangle(15, 15);
    public static Rectangle rBarra = new Rectangle(112, 24);
    public static Rectangle rMesa = new Rectangle(25, 25);
    public static Rectangle rCuadro = new Rectangle(15, 15);
    public static Rectangle rSalida = new Rectangle(26, 14);
    public static Button btnSalirEscena = new Button("Salir Escena");

    public static Rectangle rAccion = new Rectangle(15, 15);
    public static MediaPlayer mediaplayer = new MediaPlayer(new Media(new File("src/musica/Hospital.mp3").toURI().toString()));
//private ImageView personaje = new ImageView("/ImgEntrenador/M adelante1.png");

    public Hospital() {

    }

    public static void organizarElementos() {

        root.getChildren().addAll(paneHos);
        paneHos.getChildren().addAll(imgHospital, imgEnfermera, imagenJugador, rSalida);

        btnSalirEscena.setLayoutX(450);
        btnSalirEscena.setLayoutY(250);
        btnSalirEscena.setOnAction(e -> salirEscena());
        imgHospital.setLayoutY(128);
        imgHospital.setLayoutX(178);

        imgEnfermera.setLayoutX(290);
        imgEnfermera.setLayoutY(154);

        imagenJugador.setOnKeyPressed(e -> jota.movimientoJugador(imagenJugador, jota, e));

        imagenJugador.setLayoutX(291);
        imagenJugador.setLayoutY(244);
        imagenJugador.setFitHeight(19);
        imagenJugador.setFitWidth(16);

        rSalida.setLayoutX(286);
        rSalida.setLayoutY(267);

        btnInteraccion.setLayoutX(450);
        btnInteraccion.setLayoutY(250);
        btnInteraccion.setOnAction(e -> mostrarOpciones());
        btnComprar.setOnAction(e -> mostrarTienda());
        btnSanar.setOnAction(e -> sanarPokemons());
        btnSalir.setOnAction(e -> salir());

        panelOpciones.getChildren().addAll(btnComprar, btnSanar, btnSalir);
        //panelOpciones.getChildren().addAll(darDinero);                 Comprobar que compra cuando hay dinero
        //darDinero.setOnAction(e->Main.getJugador().setDinero(Main.getJugador().getDinero()+100));
        panelOpciones.setLayoutX(450);
        panelOpciones.setLayoutY(100);

        panelTienda.getChildren().addAll(lblDinero, lblTienda, btnPokebolas, btnSalirTienda);
        btnSalirTienda.setOnAction(e -> cerrarTienda());
        btnPokebolas.setOnAction(e -> comprarPokebola());

        compraRealizada.getChildren().addAll(lblCompra, btnCerrarCompra); // Panel De la compra Realizada

        btnOk.setOnAction(e -> cerrarMensaje());
        btnCSinDinero.setOnAction(e -> cerrarCompraRechazada());

        panelTienda.setLayoutX(450);
        panelTienda.setLayoutY(100);

        compraRechazada.getChildren().addAll(lblSinDinero, btnCSinDinero);
        panelMensaje.getChildren().addAll(lblSanar, btnOk);

        btnCerrarCompra.setOnAction(e -> cerrarCompraAceptada());//El boton Ceirra la compra y muestra de nuevo la tienda 
        panelMensaje.setLayoutX(200);
        panelMensaje.setLayoutY(85);

        rAccion.setLayoutX(291);
        rAccion.setLayoutY(175);
        rAccion.setFill(TRANSPARENT);

    }

    public static void organizarObstaculos() {
        rArriba.setFill(TRANSPARENT);
        rIzquierda.setFill(TRANSPARENT);
        rAbajo.setFill(TRANSPARENT);
        rDerecha.setFill(TRANSPARENT);

        rBorde1.setFill(TRANSPARENT);
        rBorde2.setFill(TRANSPARENT);
        rCuadro.setFill(TRANSPARENT);
        rBarra.setFill(TRANSPARENT);
        rMesa.setFill(TRANSPARENT);
        paneHos.getChildren().addAll(rArriba, rIzquierda, rDerecha, rAbajo, rBarra, rBorde1, rBorde2, rMesa, rCuadro);
        rBarra.setLayoutX(245);
        rBarra.setLayoutY(156);
        rArriba.setLayoutX(180);
        rArriba.setLayoutY(146);
        rAbajo.setLayoutX(180);
        rAbajo.setLayoutY(272);
        rIzquierda.setLayoutX(170);
        rIzquierda.setLayoutY(130);
        rDerecha.setLayoutX(420);
        rDerecha.setLayoutY(130);

        rBorde1.setLayoutX(180);
        rBorde1.setLayoutY(257.5);

        rBorde2.setLayoutX(405);
        rBorde2.setLayoutY(257.5);

        rMesa.setLayoutX(357);
        rMesa.setLayoutY(225);

        rCuadro.setLayoutX(293);
        rCuadro.setLayoutY(192);
    }

    public static void crearEscena() {
        jota = Main.deserializarJugador("jugador.ser");
        k = k + 2;
        organizarObstaculos();
        organizarElementos();
        Main.window.setScene(Main.sHos);
        imagenJugador.setLayoutY(imagenJugador.getLayoutY() - k);
        mediaplayer.play();
    }

    public static void salirEscena() {
        root.getChildren().clear();
        paneHos.getChildren().clear();
        panelTienda.getChildren().clear();
        panelOpciones.getChildren().clear();
        compraRealizada.getChildren().clear();
        compraRechazada.getChildren().clear();
        panelMensaje.getChildren().clear();
        Mundo.root.getChildren().clear();

        Mundo.cespedPoke.clear();
        piso.getChildren().remove(c1.getPanelCesped());
        piso.getChildren().remove(c2.getPanelCesped());
        piso.getChildren().remove(c3.getPanelCesped());
        Cesped c11 = new Cesped();
        Cesped c12 = new Cesped();
        Cesped c13 = new Cesped();
        Mundo.cespedPoke.add(c11);
        Mundo.cespedPoke.add(c12);
        Mundo.cespedPoke.add(c13);
        piso.getChildren().addAll(c11.getPanelCesped(), c12.getPanelCesped(), c13.getPanelCesped());
        c11.getPanelCesped().setLayoutX(402.5);
        c11.getPanelCesped().setLayoutY(277.5);
        c12.getPanelCesped().setLayoutX(77.5);
        c12.getPanelCesped().setLayoutY(327.5);
        c13.getPanelCesped().setLayoutX(477.5);
        c13.getPanelCesped().setLayoutY(52.5);
        Mundo.imagenJugador.setLayoutX(336);
        Mundo.imagenJugador.setLayoutY(322);
        Main.serializarJugador("jugador.ser", jota);
        Mundo.crearEscena();
        mediaplayer.stop();
    }

    public static void mostrarOpciones() {
        imagenJugador.setFocusTraversable(false);
        paneHos.getChildren().remove(btnInteraccion);
        paneHos.getChildren().add(panelOpciones);

    }

    public static void mostrarTienda() {
        paneHos.getChildren().removeAll(panelOpciones, btnInteraccion);

        paneHos.getChildren().add(panelTienda);

    }

    public static void cerrarTienda() {
        paneHos.getChildren().remove(panelTienda);
        paneHos.getChildren().add(panelOpciones);
    }

    public static void sanarPokemons() {

        for (Pokemon p : jota.getPokemonesObtenidos()) {
            p.setHp(p.getHpMax());
        }

        paneHos.getChildren().addAll(panelMensaje);
        paneHos.getChildren().remove(panelOpciones);
    }

    public static void salir() {
        paneHos.getChildren().remove(panelOpciones);
        paneHos.getChildren().remove(rAccion);
        btnInteraccion.setDisable(true);
        imagenJugador.setFocusTraversable(true);
    }

    public static void cerrarCompraAceptada() {
        paneHos.getChildren().remove(compraRealizada);
        paneHos.getChildren().add(panelTienda);
    }

    public static void cerrarMensaje() {
        paneHos.getChildren().remove(panelMensaje);
        paneHos.getChildren().add(btnInteraccion);
    }

    public static StackPane getRoot() {
        return root;

    }

    public static Pane getPane() {
        return paneHos;
    }

    public static Button getButton() {
        return btnInteraccion;
    }

    public static void cerrarCompraRechazada() {
        paneHos.getChildren().remove(compraRechazada);
        paneHos.getChildren().add(panelTienda);
    }

    public static void comprarPokebola() {
        paneHos.getChildren().removeAll(panelTienda);

        if (jota.getDinero() >= 100) {
            paneHos.getChildren().add(compraRealizada);
            jota.setPokeballs(jota.getPokeballs() + 1);
            jota.setDinero(jota.getDinero() - 100);

        } else {
            paneHos.getChildren().add(compraRechazada);
        }
    }

}
