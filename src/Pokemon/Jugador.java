/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pokemon;

import Batalla.Batalla;
import GUIs.Gym;
import static GUIs.Gym.imgGym;
import GUIs.Hospital;
import GUIs.Mundo;
import Main.Main;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import static javafx.scene.paint.Color.BLACK;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Angie
 */
public class Jugador extends Entrenador implements Serializable {

    private Pokemon pokeInicial;
    private boolean campeon;
    private int dinero, pokeballs;
    private String imagePathAdel1, imagePathDer1, imagePathIzq1, imagePathAtras1;
    private String imagePathAdel2, imagePathDer2, imagePathIzq2, imagePathAtras2;
    private boolean cambio = false;
    private ArrayList<Pokemon> pokemonesObtenidos;
    private boolean cambiaEscenaGym = false;
    private boolean cambiaEscenaHosp = false;

    public Jugador() {

    }

    public Jugador(String nombre, String genero) {
        super(nombre, genero);
        this.campeon = false;
        this.dinero = 0;

    }

    public Jugador(Pokemon pokeInicial, boolean campeon, int dinero, int pokeballs, ArrayList<Pokemon> pokemonesObtenidos, String nombre, String genero) {
        super(nombre, genero);
        this.pokeInicial = pokeInicial;
        this.campeon = campeon;
        this.dinero = dinero;
        this.pokeballs = pokeballs;
        this.pokemonesObtenidos = pokemonesObtenidos;

    }

    
    
    public void movimientoJugador(ImageView imagenJugador, Jugador j, KeyEvent e) {
        switch (e.getCode()) {
            case UP:
                imagenJugador.setImage(new Image(j.getImagePathAtras1()));
                imagenJugador.setLayoutY(imagenJugador.getLayoutY() - 8);
                
                System.out.println("X: " + imagenJugador.getLayoutX() + ",Y: " + imagenJugador.getLayoutY());

                if (Main.window.getScene() == Main.sGym) {
                    revisarColisionesBordeTop(imagenJugador, Gym.rArriba);
                    if (isCollision(imagenJugador, Gym.r1) | isCollision(imagenJugador, Gym.r2) | isCollision(imagenJugador, Gym.rLider)) {
                        imagenJugador.setLayoutY(imagenJugador.getLayoutY() + 8);
                    }
                    System.out.println("algo " + imgGym.getLayoutY());
                }
                if (Main.window.getScene() == Main.sHos) {
                    revisarColisionesBordeTop(imagenJugador, Hospital.rArriba);
                    if (isCollision(imagenJugador, Hospital.rBarra) | isCollision(imagenJugador, Hospital.rBorde1) | isCollision(imagenJugador, Hospital.rBorde2) | isCollision(imagenJugador, Hospital.rMesa)) {
                        imagenJugador.setLayoutY(imagenJugador.getLayoutY() + 8);
                    }
                accionHospital(imagenJugador,Hospital.rAccion);
                }
                if (Main.window.getScene() == Main.sMun) {
                    revisarColisionesBordeTop(imagenJugador, Mundo.rBordeTop);
                    revisaColisionesCesped(imagenJugador, Mundo.cespedPoke, j);
                    revisarColisionesPuertaGym(imagenJugador, Mundo.rPuertaGym);
                    revisarColisionesPuertaHos(imagenJugador, Mundo.rPuertaHosp);
                    if (isCollision(imagenJugador, Mundo.rHosp) | isCollision(imagenJugador, Mundo.rCasa) | isCollision(imagenJugador, Mundo.rGym) | isCollision(imagenJugador, Mundo.rLake)) {
                        imagenJugador.setLayoutY(imagenJugador.getLayoutY() + 5);
                    }

                    for (Rectangle arbol : Mundo.rArbol) {
                        if (isCollision(imagenJugador, arbol)) {
                            imagenJugador.setLayoutY(imagenJugador.getLayoutY() + 5);
                        }
                    }
                }

                break;

            case DOWN:
                imagenJugador.setImage(new Image(j.getImagePathAdel1()));
                imagenJugador.setLayoutY(imagenJugador.getLayoutY() + 8);
                
                System.out.println("X: " + imagenJugador.getLayoutX() + ",Y: " + imagenJugador.getLayoutY());
                if (Main.window.getScene() == Main.sGym) {
                    revisarColisionesBordeBot(imagenJugador, Gym.rAbajo);
                    if (isCollision(imagenJugador, Gym.r1) | isCollision(imagenJugador, Gym.r2) | isCollision(imagenJugador, Gym.rLider)) {
                        imagenJugador.setLayoutY(imagenJugador.getLayoutY() - 8);
                    }
                    if (isCollision(imagenJugador, Gym.rSalida)) {
                        Gym.salirEscena();
                        Mundo.crearEscena();
                    }
                }
                if (Main.window.getScene() == Main.sHos) {
                    revisarColisionesBordeBot(imagenJugador, Hospital.rAbajo);
                    if (isCollision(imagenJugador, Hospital.rBarra) | isCollision(imagenJugador, Hospital.rBorde1) | isCollision(imagenJugador, Hospital.rBorde2) | isCollision(imagenJugador, Hospital.rMesa)) {
                        imagenJugador.setLayoutY(imagenJugador.getLayoutY() - 8);
                    }
                    if (isCollision(imagenJugador, Hospital.rSalida)) {
                        Hospital.salirEscena();
                        Mundo.crearEscena();
                    }
                
                }
                if (Main.window.getScene() == Main.sMun) {
                    revisarColisionesBordeBot(imagenJugador, Mundo.rBordeBot);
                    revisaColisionesCesped(imagenJugador, Mundo.cespedPoke, j);
                    revisarColisionesPuertaGym(imagenJugador, Mundo.rPuertaGym);
                    revisarColisionesPuertaHos(imagenJugador, Mundo.rPuertaHosp);
                    if (isCollision(imagenJugador, Mundo.rHosp) | isCollision(imagenJugador, Mundo.rCasa) | isCollision(imagenJugador, Mundo.rGym) | isCollision(imagenJugador, Mundo.rLake)) {
                        imagenJugador.setLayoutY(imagenJugador.getLayoutY() - 5);
                    }
                    for (Rectangle arbol : Mundo.rArbol) {
                        if (isCollision(imagenJugador, arbol)) {
                            imagenJugador.setLayoutY(imagenJugador.getLayoutY() - 5);
                        }
                    }
                }

                break;

                
            case RIGHT:
                imagenJugador.setImage(new Image(j.getImagePathDer1()));
                imagenJugador.setLayoutX(imagenJugador.getLayoutX() + 8);
                
                System.out.println("X: " + imagenJugador.getLayoutX() + ",Y: " + imagenJugador.getLayoutY());
                if (Main.window.getScene() == Main.sGym) {
                    revisarColisionesBordeRight(imagenJugador, Gym.rDerecha);
                    if (isCollision(imagenJugador, Gym.r1) | isCollision(imagenJugador, Gym.r2) | isCollision(imagenJugador, Gym.rLider)) {
                        imagenJugador.setLayoutX(imagenJugador.getLayoutX() - 8);
                    }
                }
                if (Main.window.getScene() == Main.sHos) {
                    revisarColisionesBordeRight(imagenJugador, Hospital.rDerecha);
                    if (isCollision(imagenJugador, Hospital.rBarra) | isCollision(imagenJugador, Hospital.rBorde1) | isCollision(imagenJugador, Hospital.rBorde2) | isCollision(imagenJugador, Hospital.rMesa)) {
                        imagenJugador.setLayoutX(imagenJugador.getLayoutX() - 8);
                    }
                
                }
                if (Main.window.getScene() == Main.sMun) {
                    revisarColisionesBordeRight(imagenJugador, Mundo.rBordeRight);
                    revisaColisionesCesped(imagenJugador, Mundo.cespedPoke, j);
                    revisarColisionesPuertaGym(imagenJugador, Mundo.rPuertaGym);
                    revisarColisionesPuertaHos(imagenJugador, Mundo.rPuertaHosp);
                    if (isCollision(imagenJugador, Mundo.rHosp) | isCollision(imagenJugador, Mundo.rCasa) | isCollision(imagenJugador, Mundo.rGym) | isCollision(imagenJugador, Mundo.rLake)) {
                        imagenJugador.setLayoutX(imagenJugador.getLayoutX() - 5);
                    }

                    for (Rectangle arbol : Mundo.rArbol) {
                        if (isCollision(imagenJugador, arbol)) {
                            imagenJugador.setLayoutX(imagenJugador.getLayoutX() - 5);
                        }
                    }
                }

                break;
            case LEFT:
                
                imagenJugador.setImage(new Image(j.getImagePathIzq1()));
                imagenJugador.setLayoutX(imagenJugador.getLayoutX() - 8);
                
                System.out.println("X: " + imagenJugador.getLayoutX() + ",Y: " + imagenJugador.getLayoutY());

                if (Main.window.getScene() == Main.sGym) {
                    revisarColisionesBordeLeft(imagenJugador, Gym.rIzquierda);
                    if (isCollision(imagenJugador, Gym.r1) | isCollision(imagenJugador, Gym.r2) | isCollision(imagenJugador, Gym.rLider)) {
                        imagenJugador.setLayoutX(imagenJugador.getLayoutX() + 8);
                    }
                }
                if (Main.window.getScene() == Main.sHos) {
                    revisarColisionesBordeLeft(imagenJugador, Hospital.rIzquierda);
                    if (isCollision(imagenJugador, Hospital.rBarra) | isCollision(imagenJugador, Hospital.rBorde1) | isCollision(imagenJugador, Hospital.rBorde2) | isCollision(imagenJugador, Hospital.rMesa)) {
                        imagenJugador.setLayoutX(imagenJugador.getLayoutX() + 8);
                    }
                
                }
                if (Main.window.getScene() == Main.sMun) {
                    revisarColisionesBordeLeft(imagenJugador, Mundo.rBordeLeft);
                    revisaColisionesCesped(imagenJugador, Mundo.cespedPoke, j);
                    revisarColisionesPuertaGym(imagenJugador, Mundo.rPuertaGym);
                    revisarColisionesPuertaHos(imagenJugador, Mundo.rPuertaHosp);
                    if (isCollision(imagenJugador, Mundo.rHosp) | isCollision(imagenJugador, Mundo.rCasa) | isCollision(imagenJugador, Mundo.rGym) | isCollision(imagenJugador, Mundo.rLake)) {
                        imagenJugador.setLayoutX(imagenJugador.getLayoutX() + 5);
                    }

                    for (Rectangle arbol : Mundo.rArbol) {
                        if (isCollision(imagenJugador, arbol)) {
                            imagenJugador.setLayoutX(imagenJugador.getLayoutX() + 5);
                        }
                    }
                }

                break;
                
                case S:
                Main.serializarJugador("jugador.ser", j);
                System.out.println("Jugador guardado.");
                break;
                
            

        }
    }
    
    /**
     * REVISA LAS COLISIONES ENTRE LA PUERTA DEL GYM Y EL JUGADOR
     * @param imagenJugador
     * @param rectangle 
     */
    public void revisarColisionesPuertaGym(ImageView imagenJugador, Rectangle rectangle) {
        if (isCollision(imagenJugador, rectangle)) {
            System.out.println("Entrar Gym");
            Main.window.setScene(Main.sGym);
            Mundo.salirEscena();
            Gym.crearEscena();
        }
        
    }
    public void accionHospital(ImageView imagenJugador,Rectangle rectangle){
        if(isCollision(imagenJugador,rectangle)){
            System.out.println("boton Accion");
            if(!Hospital.paneHos.getChildren().contains(Hospital.rAccion)){
                Hospital.paneHos.getChildren().add(Hospital.rAccion);
                Hospital.paneHos.getChildren().add(Hospital.btnInteraccion);
                
                System.out.println("HASKDLAD");
                
        }
    }
        if(!isCollision(imagenJugador,rectangle)){
            Hospital.btnInteraccion.setDisable(true);
        }
        if(isCollision(imagenJugador,rectangle)){
            Hospital.btnInteraccion.setDisable(false);
        }
    }
    /**
     * REVISA LAS COLISIONES ENTRE LA PUERTA DEL HOSPITAL Y EL JUGADOR
     * @param imagenJugador
     * @param rectangle 
     */
    public void revisarColisionesPuertaHos(ImageView imagenJugador, Rectangle rectangle) {
        if (isCollision(imagenJugador, rectangle)) {
            
            System.out.println("Entrar Hospital");
            Main.window.setScene(Main.sHos);
            Mundo.salirEscena();
            Hospital.crearEscena();
        }
    }
    
    
    /**
     * El metodo verifica las colisiones entre el jugador y el cesped, para despues iniciar la batalla
     * @param imagenJugador
     * @param cesped
     * @param j 
     */
    public void revisaColisionesCesped(ImageView imagenJugador, ArrayList<Cesped> cesped, Jugador j) {
        Iterator<Cesped> itCesped = cesped.iterator();
        while (itCesped.hasNext()) {
            Cesped c2 = itCesped.next();
            if (isCollision(c2.getPanelCesped(), imagenJugador)) {
                if (c2.getPanelCesped().getChildren().size() == 2) {
                    c2.getPanelCesped().getChildren().remove(1);
                    c2.getPanelCesped().getChildren().get(0).setScaleX(1.5);
                    c2.getPanelCesped().getChildren().get(0).setScaleY(1.5);
                } else {
                    c2.getPanelCesped().getChildren().get(0).setScaleX(1.5);
                    c2.getPanelCesped().getChildren().get(0).setScaleY(1.5);
                }
                Mundo.salirEscena();
                Batalla battle1 = new Batalla(j,c2.getPokemon());
                Scene battleScene = new Scene(battle1.getRoot(),600,400);
                Main.window.setScene(battleScene);
                System.out.println("LUCHANDO CONTRA " + c2.getPokemon());
            }
        }
    }

    public void revisarColisionesBordeTop(ImageView imagenJugador, Rectangle bordetop) {
        if (isCollision(imagenJugador, bordetop)) {
            imagenJugador.setLayoutY(imagenJugador.getLayoutY() + 8);
        }
    }

    public void revisarColisionesBordeLeft(ImageView imagenJugador, Rectangle bordeleft) {
        if (isCollision(imagenJugador, bordeleft)) {
            imagenJugador.setLayoutX(imagenJugador.getLayoutX() + 8);
        }
    }

    public void revisarColisionesBordeRight(ImageView imagenJugador, Rectangle borderight) {
        if (isCollision(imagenJugador, borderight)) {
            imagenJugador.setLayoutX(imagenJugador.getLayoutX() - 8);
        }
    }

    public void revisarColisionesBordeBot(ImageView imagenJugador, Rectangle bordebot) {
        if (isCollision(imagenJugador, bordebot)) {
            imagenJugador.setLayoutY(imagenJugador.getLayoutY() - 8);
        }
    }

    /**
     * Detecta la colision entre dos nodos
     *
     * @param n1
     * @param n2
     * @return
     */
    public static boolean isCollision(Node n1, Node n2) {
        Bounds b1 = n1.getBoundsInParent();
        Bounds b2 = n2.getBoundsInParent();
        if (b1.intersects(b2)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean colisionesObjetos(ImageView imagenJugador) {
        if (isCollision(imagenJugador, Hospital.rCuadro) | isCollision(imagenJugador, Hospital.rMesa) | isCollision(imagenJugador, Hospital.rBorde1) | isCollision(imagenJugador, Hospital.rBorde2) | isCollision(imagenJugador, Hospital.rBarra)) {
            if (isCollision(imagenJugador, Hospital.rCuadro)) {

                System.out.println("colision CUADRADO");
                if (!Hospital.getPane().getChildren().contains(Hospital.getButton())) {
                    Hospital.getPane().getChildren().add(Hospital.getButton());
                }

            }

            return true;
        }
        if (isCollision(imagenJugador, Gym.r1) | isCollision(imagenJugador, Gym.r2) | isCollision(imagenJugador, Gym.rLider)) {
            return true;
        }

        return false;
    }

    public int getPokeballs() {
        return pokeballs;
    }

    public ArrayList<Pokemon> getPokemonesObtenidos() {
        return pokemonesObtenidos;
    }

    public void setPokeInicial(Pokemon pokeInicial) {
        this.pokeInicial = pokeInicial;
    }

    public void setCampeon(boolean campeon) {
        this.campeon = campeon;
    }

    public void setDinero(int dinero) {
        this.dinero = dinero;
    }

    public void setPokeballs(int pokeballs) {
        this.pokeballs = pokeballs;
    }

    public void setPokemonesObtenidos(ArrayList<Pokemon> pokemonesObtenidos) {
        this.pokemonesObtenidos = pokemonesObtenidos;
    }

    public Pokemon getPokeInicial() {
        return pokeInicial;
    }

    public boolean isCampeon() {
        return campeon;
    }

    public int getDinero() {
        return dinero;
    }

    public void setCambio(boolean cambio) {
        this.cambio = cambio;
    }

    public boolean getCambio() {
        return this.cambio;
    }

    public void setImagePathAdel1(String imagePathAdel1) {
        this.imagePathAdel1 = imagePathAdel1;
    }

    public void setImagePathDer1(String imagePathDer1) {
        this.imagePathDer1 = imagePathDer1;
    }

    public void setImagePathIzq1(String imagePathIzq1) {
        this.imagePathIzq1 = imagePathIzq1;
    }

    public void setImagePathAtras1(String imagePathAtras1) {
        this.imagePathAtras1 = imagePathAtras1;
    }

    public void setImagePathAdel2(String imagePathAdel2) {
        this.imagePathAdel2 = imagePathAdel2;
    }

    public void setImagePathDer2(String imagePathDer2) {
        this.imagePathDer2 = imagePathDer2;
    }

    public void setImagePathIzq2(String imagePathIzq2) {
        this.imagePathIzq2 = imagePathIzq2;
    }

    public void setImagePathAtras2(String imagePathAtras2) {
        this.imagePathAtras2 = imagePathAtras2;
    }

    public String getImagePathAdel1() {
        return imagePathAdel1;
    }

    public String getImagePathDer1() {
        return imagePathDer1;
    }

    public String getImagePathIzq1() {
        return imagePathIzq1;
    }

    public String getImagePathAtras1() {
        return imagePathAtras1;
    }

    public String getImagePathAdel2() {
        return imagePathAdel2;
    }

    public String getImagePathDer2() {
        return imagePathDer2;
    }

    public String getImagePathIzq2() {
        return imagePathIzq2;
    }

    public String getImagePathAtras2() {
        return imagePathAtras2;
    }

    public boolean isCambiaEscenaGym() {
        return cambiaEscenaGym;
    }

    public boolean isCambiaEscenaHosp() {
        return cambiaEscenaHosp;
    }

    @Override
    public void setNombre(String nombre) {
        super.setNombre(nombre);
    }

    @Override
    public void setGenero(String genero) {
        super.setGenero(genero);
    }

    @Override
    public String getNombre() {
        return super.getNombre();
    }

    @Override
    public String getGenero() {
        return super.getGenero();
    }

}
