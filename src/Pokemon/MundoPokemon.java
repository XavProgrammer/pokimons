/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pokemon;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javafx.scene.Scene;

/**
 *
 * @author Angellina
 */
public class MundoPokemon implements Serializable{
    static ArrayList<Pokemon> pokemonesDisponibles = lecturaPokemones("pokemon.csv");
    Jugador j;
    Scene ultimaEscena;
    public MundoPokemon(){
        
    }
    public MundoPokemon(Jugador j){
        this.j =j;
        
    }
    public ArrayList<Pokemon> deserializarPartida(String filename) {
        ArrayList<Pokemon> puchamones = null;
        try (ObjectInputStream reader = new ObjectInputStream(new FileInputStream(filename))) {
            puchamones = (ArrayList<Pokemon>)reader.readObject();
        } catch (IOException ex) {
            System.out.println("Problemas con " + ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("NO EXISTE CLASE POKEMON");
        }
        return puchamones;
    }

   
    /**
     * Lee el un archivo que contiene los pokemones con el formato que posee "pokemones.csv"
     * y retorna los pokemones de la preimera generacion en un ArrayyList
     * @param filename
     * @return ArrayList puchamones
     */
    public static ArrayList<Pokemon> lecturaPokemones(String filename) {
        List<Pokemon> puchamones = new ArrayList<>();
        int n=0;
        String nombre,tipo1,tipo2;
        String[] ataques;
        double against_bug,against_dark, against_dragon, against_electric, 
            against_fairy, against_fight, against_fire, against_flying, 
            against_ghost, against_grass, against_ground, against_ice, against_normal, 
            against_poison, against_psychic, against_rock, against_steel, against_water;
        int attack, defense, capture_rate, pokedex_num;
        double hp, hpMax;
        int exp;
        boolean is_legendary;
        try {
            List<String> lineas = Files.readAllLines(Paths.get(filename));
            lineas.remove(0);
            for (String linea: lineas){
                //System.out.println(linea);
                
                
                linea.replace("\\'","");
                String [] lc = linea.split("\\[");
                
               
                String [] lata= lc[1].split("\\]");
                ataques = lata[0].split(",");
                String [] l = lata[1].split(",");
                
                against_bug = Double.parseDouble(l[1]);
                against_dark = Double.parseDouble(l[2]);
                against_dragon = Double.parseDouble(l[3]);
                against_electric = Double.parseDouble(l[4]);
                against_fairy = Double.parseDouble(l[5]);
                against_fight = Double.parseDouble(l[6]);
                against_fire = Double.parseDouble(l[7]);
                against_flying = Double.parseDouble(l[8]); 
                against_ghost = Double.parseDouble(l[9]);
                against_grass = Double.parseDouble(l[10]);
                against_ground = Double.parseDouble(l[11]);
                against_ice = Double.parseDouble(l[12]);
                against_normal = Double.parseDouble(l[13]); 
                against_poison = Double.parseDouble(l[14]);
                against_psychic = Double.parseDouble(l[15]);
                against_rock = Double.parseDouble(l[16]);
                against_steel = Double.parseDouble(l[17]);
                against_water = Double.parseDouble(l[18]);
                attack = Integer.parseInt(l[19]);
                capture_rate = Integer.parseInt(l[20]);
                defense = Integer.parseInt(l[21]);
                hp = Integer.parseInt(l[22]);
                nombre = l[23];
                pokedex_num = Integer.parseInt(l[24]);
                tipo1 = l[25] ;
                tipo2 = l[26];
                hpMax = Integer.parseInt(l[22]);
                is_legendary = Boolean.parseBoolean(l[28]);
                
                Pokemon p = new Pokemon(ataques, tipo1, tipo2 ,nombre ,against_bug, against_dark, against_dragon, against_electric,against_fairy, against_fight, against_fire, against_flying, against_ghost, against_grass, against_ground, against_ice, against_normal,against_poison, against_psychic, against_rock, against_steel, against_water, attack, defense, capture_rate, pokedex_num, hp, is_legendary,hpMax);
                puchamones.add(p);
                
                
               
                n++;
                if (n==151)
                    break;
                
                
                
            }
        } catch (IOException ex) {
            System.err.println("Error al leer" + ex);
        }
        
        return (ArrayList<Pokemon>) puchamones;
    }

       
    /**
     * Devuelve el arreglo de pokemones disponibles
     * @return pokemonesDisponibles 
     */
    public static ArrayList<Pokemon> getPokemonesDisponibles() {
        return pokemonesDisponibles;
    }
    /**
     * Devuelve al jugador del MundoPokemon
     * @return Jugador j
     */

    public Jugador getJugador() {
        return j;
    }
    /**
     * Devuelve la ultima escena
     * @return Scene ultimaEscena
     */
    public Scene getUltimaEscena() {
        return ultimaEscena;
    }
    /**
     * Settea al Jugador
     * @param j 
     */
    public void setJugador(Jugador j) {
        this.j = j;
    }
    /**
     * Settea la ultima escena
     * @param ultimaEscena 
     */
    public void setUltimaEscena(Scene ultimaEscena) {
        this.ultimaEscena = ultimaEscena;
    }
    public Pokemon getPokemon(String nombrePoke){
        Pokemon puchamon = null;
        for(Pokemon p : pokemonesDisponibles){
            if (p.getNombre().equals(nombrePoke)){
                puchamon=p;
            }
        }
        return puchamon;
        
    }
    
    
}
