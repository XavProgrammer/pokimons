/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pokemon;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Angie
 */
public  class Entrenador implements Serializable{
    private String nombre, genero;
    private ArrayList<Pokemon> listaPokemon;
    public Entrenador(){
        
    }
    public Entrenador(String nombre, String genero){
        this.nombre= nombre;
        this.genero = genero;
    }

    public Entrenador(String nombre, String genero, ArrayList<Pokemon> listaPokemon) {
        this.nombre = nombre;
        this.genero = genero;
        this.listaPokemon = listaPokemon;
    }

    
    
    public String getNombre() {
        return nombre;
    }

    public String getGenero() {
        return genero;
    }

    public ArrayList<Pokemon> getListaPokemon() {
        return listaPokemon;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setListaPokemon(ArrayList<Pokemon> listaPokemon) {
        this.listaPokemon = listaPokemon;
    }

    

    
    
}
