/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pokemon;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Angellina
 */
public class Pokemon implements Serializable{
    private int exp;
    private String nombre, tipo1, tipo2;
    private String[] ataques;
    private double against_bug,	against_dark, against_dragon, against_electric, 
            against_fairy, against_fight, against_fire, against_flying, 
            against_ghost, against_grass, against_ground, against_ice, against_normal, 
            against_poison, against_psychic, against_rock, against_steel, against_water;
    private int attack, defense, capture_rate, pokedex_num;
    private double hp, hpMax;
    private boolean is_legendary;
    private String rutaFrente;
    private String rutaEspalda;

        
    /**
     * Agrega las rutas de las imagenes de frente y espalda de los pokemones
     */
    public void rutaPokemon(){
        String numpokedex = String.valueOf(pokedex_num);
        this.rutaFrente = "/ImgPokeFrente/"+numpokedex+".png";
        this.rutaEspalda = "/ImgPokeEspalda/"+numpokedex+".png";
    }
    
    
    /**
     * Sobreestcitura del metodo toString
     * @return 
     */
    public String toString(){
        return this.nombre+","+this.pokedex_num+","+this.ataques+","+this.against_bug+","+this.against_dark+","+this.against_dragon+","+this.against_electric+","+this.against_fairy+","+this.against_fight+","+this.against_fire+","+this.against_flying+","+this.against_ghost+","+this.against_grass+","+this.against_ground+","+this.against_ice+","+this.against_normal+","+this.against_poison+","+this.against_psychic+","+this.against_rock+","+this.against_steel+","+this.against_water+"\n";
    }
    /**
     * Set del nombre
     * @param nombre 
     */
   
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * set del numero del pokedex
     * @param pokedex_num 
     */
    public void setPokedex_num(int pokedex_num) {
        this.pokedex_num = pokedex_num;
    }
    /**
     * Constructor de Pokemon
     * @param ataques
     * @param tipo1
     * @param tipo2
     * @param nombre
     * @param against_bug
     * @param against_dark
     * @param against_dragon
     * @param against_electric
     * @param against_fairy
     * @param against_fight
     * @param against_fire
     * @param against_flying
     * @param against_ghost
     * @param against_grass
     * @param against_ground
     * @param against_ice
     * @param against_normal
     * @param against_poison
     * @param against_psychic
     * @param against_rock
     * @param against_steel
     * @param against_water
     * @param attack
     * @param defense
     * @param capture_rate
     * @param pokedex_num
     * @param hp
     * @param is_legendary
     * @param hpMax 
     */

    public Pokemon(String[] ataques, String tipo1, String tipo2, String nombre, double against_bug, double against_dark, double against_dragon, double against_electric, double against_fairy, double against_fight, double against_fire, double against_flying, double against_ghost, double against_grass, double against_ground, double against_ice, double against_normal, double against_poison, double against_psychic, double against_rock, double against_steel, double against_water, int attack, int defense, int capture_rate, int pokedex_num, double hp, boolean is_legendary, double hpMax) {
        this.exp = 5;
        this.ataques = ataques;
        this.tipo1 = tipo1;
        this.tipo2 = tipo2;
        this.nombre = nombre;
        this.against_bug = against_bug;
        this.against_dark = against_dark;
        this.against_dragon = against_dragon;
        this.against_electric = against_electric;
        this.against_fairy = against_fairy;
        this.against_fight = against_fight;
        this.against_fire = against_fire;
        this.against_flying = against_flying;
        this.against_ghost = against_ghost;
        this.against_grass = against_grass;
        this.against_ground = against_ground;
        this.against_ice = against_ice;
        this.against_normal = against_normal;
        this.against_poison = against_poison;
        this.against_psychic = against_psychic;
        this.against_rock = against_rock;
        this.against_steel = against_steel;
        this.against_water = against_water;
        this.attack = attack;
        this.defense = defense;
        this.capture_rate = capture_rate;
        this.pokedex_num = pokedex_num;
        this.hp = hp;
        this.is_legendary = is_legendary;
        this.hpMax = hpMax;
        rutaPokemon();
        
    }    
    
    public String getRutaFrente() {
        return rutaFrente;
    }
    
    
    public String getRutaEspalda() {
        return rutaEspalda;
    }

   
    public int getExp() {
        return exp;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public String getTipo1() {
        return tipo1;
    }
    
    public String getTipo2() {
        return tipo2;
    }
    
    public String[] getAtaques() {
        return ataques;
    }
    
    public double getAgainst_bug() {
        return against_bug;
    }
    
    public double getAgainst_dark() {
        return against_dark;
    }
    
    public double getAgainst_dragon() {
        return against_dragon;
    }
    
    public double getAgainst_electric() {
        return against_electric;
    }
    
    public double getAgainst_fairy() {
        return against_fairy;
    }
    
    public double getAgainst_fight() {
        return against_fight;
    }
    
    public double getAgainst_fire() {
        return against_fire;
    }
   
    public double getAgainst_flying() {
        return against_flying;
    }
  
    public double getAgainst_ghost() {
        return against_ghost;
    }
    
    public double getAgainst_grass() {
        return against_grass;
    }
    
    public double getAgainst_ground() {
        return against_ground;
    }
  
    public double getAgainst_ice() {
        return against_ice;
    }
    
    public double getAgainst_normal() {
        return against_normal;
    }
    
    public double getAgainst_poison() {
        return against_poison;
    }

    public double getAgainst_psychic() {
        return against_psychic;
    }

    public double getAgainst_rock() {
        return against_rock;
    }

    public double getAgainst_steel() {
        return against_steel;
    }

    public double getAgainst_water() {
        return against_water;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

    public int getCapture_rate() {
        return capture_rate;
    }

    public int getPokedex_num() {
        return pokedex_num;
    }

    public double getHp() {
        return hp;
    }

    public boolean isIs_legendary() {
        return is_legendary;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    public double getHpMax(){
        return hpMax;
    }
    
}
