/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pokemon;

import Batalla.Batalla;
import GUIs.Mundo;
import Main.Main;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Angie
 */
public class Dominguero extends Entrenador implements Runnable {
    Random r = new Random();
    private int segundos;
    private int respawnTime = 50;
    private String rutaImagen = "/ImgEntrenador/DomingueraMini.png";
    boolean isMundo;
    private ImageView imagenDom = new ImageView(new Image(rutaImagen));
    
    /**
     * Constructor de Dominguero
     * @param nombre
     * @param genero
     * @param listaPokemon
     * @param ruta 
     */
    public Dominguero(String nombre, String genero, ArrayList<Pokemon> listaPokemon, String ruta) {
        super(nombre, genero, listaPokemon);
        this.rutaImagen = ruta;
    }


    public int getRespawnTime() {
        return respawnTime;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    @Override
    /**
     * Lanza a un dominguero cada 50 segundos
     */
    public void run() {
        segundos = 1;
        isMundo = checkOtherScene();
        if (isMundo) {
            while (isMundo) {
                segundos++;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println(segundos);
                        if (segundos % respawnTime == 0) {

                            Mundo.piso.getChildren().add(imagenDom);
                            imagenDom.setFitHeight(30);
                            imagenDom.setFitWidth(30);
                            imagenDom.setLayoutX(Mundo.imagenJugador.getLayoutX() + 10);
                            imagenDom.setLayoutY(Mundo.imagenJugador.getLayoutY() + 10);
                            System.out.println("DOMINGUERO NOW");
                            Mundo.salirEscena();
                            cambiarEscenaPelea();
                        }
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    System.out.println("ksssss");
                }

                isMundo = checkOtherScene();
                
            }
        }

    }

    /**
     * Revisa si la escena es de tipo Mundo, para así controlar a la thread
     * @return 
     */
    public boolean checkOtherScene() {
        if (Main.window.getScene() == Main.sMun) {
            return true;
        }

        return false;
    }
    /**
     * Cambia la escena de la pelea
     */
    public void cambiarEscenaPelea(){
        Batalla battle2 = new Batalla(Mundo.j, this);
        Scene battleDom = new Scene(battle2.getRoot(), 600, 400);
        
        Batalla b1 = new Batalla(Mundo.j, this);
        Scene s1 = new Scene(b1.getRoot(), 600, 400);
        Main.window.setScene(battleDom);
    }
}
