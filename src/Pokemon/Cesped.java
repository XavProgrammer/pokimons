/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pokemon;

import java.util.ArrayList;
import java.util.Random;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Angellina
 */
public class Cesped {

    Random r = new Random();
    private StackPane panelCesped = new StackPane();
    private ImageView imagenCesped = new ImageView(new Image("/ImgEscenario/cespedsolo.png"));
    private Pokemon pokemon;
    
    ArrayList<Pokemon> battalla = new ArrayList<>();
    /**
     * A un cesped le asigna una imagen y un pokemon
     */
    public Cesped() {
        battalla.add(MundoPokemon.getPokemonesDisponibles().get(0));
        battalla.add(MundoPokemon.getPokemonesDisponibles().get(9));
        battalla.add(MundoPokemon.getPokemonesDisponibles().get(12));
        battalla.add(MundoPokemon.getPokemonesDisponibles().get(12));battalla.add(MundoPokemon.getPokemonesDisponibles().get(12));battalla.add(MundoPokemon.getPokemonesDisponibles().get(12));
        battalla.add(MundoPokemon.getPokemonesDisponibles().get(18));
        battalla.add(MundoPokemon.getPokemonesDisponibles().get(18));
        battalla.add(MundoPokemon.getPokemonesDisponibles().get(49));battalla.add(MundoPokemon.getPokemonesDisponibles().get(49));
        this.pokemon = battalla.get(r.nextInt(10));
        ImageView imagenPoke = new ImageView(new Image(pokemon.getRutaFrente()));
        imagenCesped.setFitHeight(25);
        imagenCesped.setFitWidth(25);
        imagenPoke.setFitHeight(25);
        imagenPoke.setFitWidth(25);
        this.panelCesped.getChildren().addAll(imagenPoke, imagenCesped);

    }

    public StackPane getPanelCesped() {
        return this.panelCesped;
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    @Override
    public String toString() {
        return "Cesped{" + "pokemon=" + pokemon + '}';
    }
    
    
}
