/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Batalla;

import GUIs.Hospital;
import GUIs.Mundo;
import Pokemon.Dominguero;
import Pokemon.Entrenador;
import Pokemon.Jugador;
import Pokemon.Lider;
import Pokemon.Pokemon;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Font;

/**
 *
 * @author pb
 */
public class Batalla {
    public static MediaPlayer mediaplayer = new MediaPlayer(new Media(new File("src/musica/Combate.mp3").toURI().toString()));

    HBox arriba = new HBox();
    HBox abajo = new HBox();
    VBox palabras = new VBox(); //Vbox con las palabras que apareceran durante la batalla
    Label palabraA = new Label("  !!!!! ");
    
    Label palabraB = new Label(" ");
    ImageView fondo = new ImageView(new Image("/images/CampoBatalla.png"));

    Button poke = new Button("POKéMON");
    Button pokeball = new Button("Pokeball");
    Button lucha = new Button("Lucha");
    Button huir = new Button("Huida");
    
    StackPane paneTodo = new StackPane();
    VBox root = new VBox();
    HBox menu = new HBox();
    ImageView fond = new ImageView(new Image("/images/fonditoCombate.png"));
    VBox opciones = new VBox();
    ImageView espalda;
    ImageView enemigoMain;
    Button siguiente = new Button("Siguiente");
//        
//    ImageView ball1 = new ImageView(new Image("/images/pokeball1.png"));
//    ImageView ball2 = new ImageView(new Image("/images/pokeball2.png"));
    
    ImageView poke1Img;
    ImageView poke2Img;
    
    Label lblTurno;
    
    //Para que se visualicen los número del daño en formato 0.00
    DecimalFormat df = new DecimalFormat("#.00");
    
    private HBox pokeCambiar = new HBox(); //Aqui ira el nombre de los pokemon cuando el entrenador los vaya a cambiar
    private Entrenador enemigo,ganador;
//    private Pokemon pokeGanador;
//    //Pokemon del jugador **
    private Pokemon poke1; //pokemon usuario
    private Pokemon poke2;
//    //rivales
    private Jugador usuario;
    
    //Aqui se almacenan los pokemons del usuario y enemigo que no esten debilitados
    private ArrayList<Pokemon> pokeVivosJug = new ArrayList<>(); 
    private ArrayList<Pokemon> pokeVivosEn = new ArrayList<>(); 
    
    int cambioPoke = 0;
    String turno = "Jugador";
    
    /**
     * Pelea de usuario contra un enemigo dominguero
     * @param usuario Entrenador de tipo jugador
     * @param enemigo Entrenador de tipo dominguero
     */
    public Batalla(Jugador usuario, Dominguero enemigo){
        
        this.enemigo = enemigo;
        this.usuario = usuario;
                
        palabraB = new Label("¡"+enemigo.getNombre()+" quiere luchar contra ti!");
        
        palabraA.setFont(Font.font("Minecraftia", 17));
        palabraB.setFont(Font.font("Minecraftia", 17));
        
        paneTodo.getChildren().clear();
        poke2 = enemigo.getListaPokemon().get(0);
        
        organizarInicio();
        
        enemigoMain = new ImageView(new Image("/ImgEntrenador/Dominguera.png"));
        enemigoMain.setFitWidth(150);
        enemigoMain.setFitHeight(150);
        
        paneTodo.getChildren().add(enemigoMain);
        enemigoMain.setTranslateX(150);
        enemigoMain.setTranslateY(-100);
        
    }
    
    /**
     * Pelea de usuario contra lider angela
     * @param usuario
     * @param enemigo 
     */
    public Batalla(Jugador usuario,Lider enemigo){
        paneTodo.getChildren().clear();
        this.enemigo = enemigo;
        this.usuario = usuario;
        
        palabraB = new Label("¡"+enemigo.getNombre()+" quiere luchar contra ti!");
        palabraA.setFont(Font.font("Minecraftia", 17));
        palabraB.setFont(Font.font("Minecraftia", 17));
        poke2 = enemigo.getListaPokemon().get(0);
        organizarInicio();
        
        enemigoMain = new ImageView(new Image("/ImgEntrenador/Lider.png"));
        enemigoMain.setFitWidth(150);
        enemigoMain.setFitHeight(150);
        paneTodo.getChildren().add(enemigoMain);
        enemigoMain.setTranslateX(150);
        enemigoMain.setTranslateY(-100);
        
    }
    
    /**
     * Pelea de usuario contra pokemon salvaje
     * @param usuario
     * @param poke2 
     */
    public Batalla(Jugador usuario, Pokemon poke2){
        this.poke2 = poke2;
        this.usuario = usuario;
        this.enemigo = null;
        
        paneTodo.getChildren().clear();
        
        palabraB = new Label("¡"+poke2.getNombre()+" salvaje quiere luchar contra ti!");
        
        palabraA.setFont(Font.font("Minecraftia", 17));
        palabraB.setFont(Font.font("Minecraftia", 17));
        
        pokeVivosJug = usuario.getPokemonesObtenidos();
        
        organizarInicio();
        
    }
//    
//    public void animacionAtrapar(){
//        paneTodo.getChildren().add(ball1);
//    }
    
    /**
     * En un array almacena pokemons vivos
     */
    public void pokeVivos(){
        pokeVivosJug = usuario.getPokemonesObtenidos();
        pokeVivosEn = enemigo.getListaPokemon();
        System.out.println(pokeVivosEn);
        
    }
     
    
    /**
     * Organiza botones en el menu
     */
    public void organizarElementos(){
        deshabilitarBotones(false);
        siguiente.setDisable(true);
        this.poke = new Button("Pokemon");
        this.pokeball = new Button("Pokeball");
        this.lucha = new Button("Lucha");
        this.huir = new Button("Huida");
        
        poke.setFont(Font.font("Minecraftia", 15));
        pokeball.setFont(Font.font("Minecraftia", 15));
        lucha.setFont(Font.font("Minecraftia", 15));
        huir.setFont(Font.font("Minecraftia", 15));
        
        palabras.getChildren().clear();
        opciones.getChildren().clear();
        menu.getChildren().clear();
        
        palabraA.setText("¿Qué deseas hacer?");
        palabraB.setText("Escoge una opcion ->");
                
        
        lucha.setMinSize(100,50);
        pokeball.setMinSize(100,50);
        huir.setMinSize(100,50);
        poke.setMinSize(100,50);
        
        arriba.getChildren().clear();
        abajo.getChildren().clear();
        
        arriba.getChildren().addAll(lucha,pokeball);
        abajo.getChildren().addAll(poke,huir);
        opciones.getChildren().addAll(arriba,abajo);
        
        opciones.setTranslateX(300);        
        opciones.setTranslateX(150);
        
        palabras.getChildren().addAll(palabraA,palabraB);
        menu.getChildren().addAll(palabras,opciones);
        //palabras.setAlignment(Pos.BASELINE_LEFT);
        //siguiente.setDisable(true);
        
    }
    
       
    /**
     * Añade función a botones
     */
    public void crearEventos(){
        poke.setOnAction(null);
        pokeball.setOnAction(null);
        huir.setOnAction(null);
        lucha.setOnAction(null);
        
        
        poke.setOnAction(e->cambiarPokemonUsuario());
        pokeball.setOnAction(e->atraparPoke());
        huir.setOnAction(e -> huida());
        lucha.setOnAction(e->luchaPoke());
        
    }
    
    //Coloca el fondo y la imagen de espalda de jugador
    public void organizarInicio(){
        mediaplayer.play();
        
        this.poke1 = usuario.getPokemonesObtenidos().get(0);
        
        fondo.setFitWidth(600);
        fondo.setFitHeight(300);
               
        fond.setFitWidth(600);
        fond.setFitHeight(100);
        fond.setOpacity(0.25);
        
        
        menu.getChildren().clear();
        palabras.getChildren().clear();
        palabras.getChildren().addAll(palabraA,palabraB);
        palabraA.setAlignment(Pos.CENTER);
//        palabraB.setAlignment(Pos.CENTER);
        
        siguiente.setFont(Font.font("Minecraftia", 15));
        siguiente.setMinSize(100,25);
        
        
        siguiente.setTranslateX(200);
        siguiente.setTranslateY(50);
        StackPane m = new StackPane();
        m.getChildren().add(fond);
        m.getChildren().add(menu);
        menu.getChildren().addAll(palabras);
        root.getChildren().addAll(fondo,m);
                
        if(usuario.getGenero().equals("Femenino")){
            espalda = new ImageView(new Image("/ImgEntrenador/F_Espalda.png"));
        }else{
            espalda = new ImageView(new Image("/ImgEntrenador/M_Espalda.png"));
        }
        espalda.setFitWidth(150);
        espalda.setFitHeight(150); 
        espalda.setTranslateX(-150);
        espalda.setTranslateY(25);
        
        paneTodo.getChildren().add(root);
        paneTodo.getChildren().add(espalda);
        paneTodo.getChildren().add(siguiente);
        //ANIMACIÓN POKEBALL Y VAINASSS
        siguiente.setDisable(false);
        if(batallaVSEntrenador()==true){
            siguiente.setOnAction(e -> palabrasEnemigo());

            
        }else{
            siguiente.setOnAction(e -> palabrasUsuario());
        }
        
        
    }

    
    /**
     * Organiza momento en que enemigo agrega un pokemon a combate
     */
    public void palabrasEnemigo(){
//        paneTodo.getChildren().remove(siguiente);
        palabraA.setText(enemigo.getNombre()+" usa a "+ poke2.getNombre());
        menu.getChildren().clear();
        menu.getChildren().addAll(palabraA);
        //menu.setAlignment(Pos.CENTER);
//        siguiente.setTranslateX(200);
//        siguiente.setTranslateY(75);
        siguiente.setDisable(false);
        siguiente.setOnAction(null);
        siguiente.setOnAction(e ->palabrasUsuario());
//        paneTodo.getChildren().add(siguiente);
    }
        
    /**
     * Organiza momento en que usuario agrega pokemon a combate
     */
    public void palabrasUsuario(){
        palabraA.setText("¡Ve "+poke1.getNombre()+"!");
                
        menu.getChildren().clear();
        menu.getChildren().addAll(palabraA);
//        menu.setAlignment(Pos.CENTER);
//        siguiente.setTranslateX(200);
//        siguiente.setTranslateY(75);
            
        siguiente.setDisable(false);
        siguiente.setOnAction(null);
        paneTodo.getChildren().remove(espalda);
        paneTodo.getChildren().remove(enemigoMain);
        siguiente.setOnAction(e -> organizarEnemigo());
        
    }
    
    /**
     * Agrega imagen de pokemon de usuario y enemigo en la batalla
     */
    public void organizarEnemigo(){
        organizarPokeUsuario(poke1);
        if(batallaVSEntrenador() == true){
            pokeVivos();
            System.out.println("entra aqui uwu");
            poke2 = enemigo.getListaPokemon().get(0);  
        }
        System.out.println("organiza pokeEnemigo");
        organizarPokeEnemigo(poke2);
//        siguiente.setTranslateX(200);
//        siguiente.setTranslateY(75);
        siguiente.setDisable(false);
        siguiente.setOnAction(null);
        siguiente.setOnAction(e -> inicioBatalla());
        
    }
    
    
    
    /**
     * Verifica si batalla es vs entrenador o vs pokemon salvaje
     * @return true si batalla es contra entrenador o false si es contra pokemon salvaje
     */
    public boolean batallaVSEntrenador(){
        if(enemigo != null){
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * Agrega en un image View el pokemon actual del usuario, junto con su posición y tamaño
     * @param p Pokemon actual del usuario
     */
    public void organizarPokeUsuario(Pokemon p){
        poke1Img = new ImageView(new Image(p.getRutaEspalda()));
        
        poke1Img.setFitWidth(150);
        poke1Img.setFitHeight(150);
        
        poke1Img.setTranslateX(-150);
        poke1Img.setTranslateY(25);
        paneTodo.getChildren().add(poke1Img);
        System.out.println("organizar poke usuario");
    }
    
    /**
     * Agrega en un image view el pokemon actual del enemigo, junto con su posición y tamaño
     * @param p Pokemon actual del enemigo
     */
    public void organizarPokeEnemigo(Pokemon p){
        poke2Img = new ImageView(new Image(p.getRutaFrente()));
        
        poke2Img.setFitWidth(150);
        poke2Img.setFitHeight(150);
        
        poke2Img.setTranslateX(150);
        poke2Img.setTranslateY(-75);
        paneTodo.getChildren().addAll(poke2Img);
    }
    
    
    /**
     * Manejo de batalla y turnos
     */
    public void inicioBatalla(){
        //Si la batalla es vs un pokemon salvaje
        System.out.println("vida poke 2: "+poke2.getHp());
        if(batallaVSEntrenador()==false){
            if(poke2.getHp()>0){
                if("Jugador".equals(turno)){
                    //Actualizar estado para el siguiente turno
                    turno = "Enemigo";
                    lblTurno = new Label("TURNO JUGADOR");
                    lblTurno.setFont(Font.font("Minecraftia", 50));
                    paneTodo.getChildren().add(lblTurno);
                    
//                    deshabilitarBotones(true);
//                    siguiente.setTranslateX(200);
//                    siguiente.setTranslateY(75);
//                    
                    siguiente.setDisable(false);
                    siguiente.setOnAction(null);
                    siguiente.setOnAction(e -> turnoUsuario());
                    System.out.println("llega aca a jugador");
                    //paneTodo.getChildren().addAll(siguiente);
                    

                }else{
                    //Actualizar estado para el siguiente turno
                    
                    siguiente.setDisable(false);
                    turno = "Jugador";
                    System.out.println("Entra a turno enemigo desuu");
                    deshabilitarBotones(true);
                    
                    lblTurno = new Label("TURNO ENEMIGO");
                    lblTurno.setFont(Font.font("Minecraftia", 50));
                    paneTodo.getChildren().add(lblTurno);
                    
                    siguiente.setDisable(false);
                    siguiente.setOnAction(null);
                    siguiente.setOnAction(e -> enemigoAtaca());
                    
                    
                }                
            }
            if(poke2.getHp()<=0){
                System.out.println("termina batallin desu");
                //Usuario gana y pokemons 2 está debilitado
                ganador = usuario;
                palabraA.setText("¡Belleza mayonesa!");
                palabraB.setText("Has ganado 200₽");
                
                usuario.setDinero(usuario.getDinero()+200);
                palabras.getChildren().clear();
                menu.getChildren().clear();
                palabras.getChildren().addAll(palabraA,palabraB);
                menu.getChildren().add(palabras);
                siguiente.setDisable(false);
                siguiente.setOnAction(null);
                siguiente.setOnAction(e ->finPantallaMundo());
                
                        
            }
            if(pokeVivosJug.isEmpty() == true){
                System.out.println(pokeVivosJug);
                System.out.println(pokeVivosJug.isEmpty());
                //Todos los pokemons del usuario estan debilitados
                System.out.println("PORQUEEEEE");
                palabraA.setText("¡Tus pokemons se han debilitado!");
                palabraB = new Label("¡"+usuario.getNombre()+" ha perdido!");
                
                palabras.getChildren().clear();
                menu.getChildren().clear();
                palabras.getChildren().addAll(palabraA,palabraB);
                menu.getChildren().add(palabras);
                siguiente.setDisable(false);
                siguiente.setOnAction(null);
                siguiente.setOnAction(e ->finPantallaHospital());
            }
            
        }
        //Batalla contra otro jugador
        else{
            
            if(!pokeVivosJug.isEmpty()){
                if("Jugador".equals(turno)){
                    turno = "Enemigo"; //Cambia a turno del enemigo
                    System.out.println(turno);

                    lblTurno = new Label("TURNO JUGADOR");
                    lblTurno.setFont(Font.font("Minecraftia", 50));
                    paneTodo.getChildren().add(lblTurno);
                    
                    //deshabilitarBotones(true);
                    siguiente.setTranslateX(200);
                    siguiente.setTranslateY(75);
                    siguiente.setDisable(false);
                    siguiente.setOnAction(null);
                    siguiente.setOnAction(e -> turnoUsuario());
                    System.out.println("llega aca a jugador");
//                    paneTodo.getChildren().addAll(siguiente);
                    
                }else{
                    turno = "Jugador";//Cambia a turno del jugador
                    //Turno del enemigo
                    System.out.println(turno);

                    //siguiente.setDisable(false);
//                    deshabilitarBotones(true);
                    
                    lblTurno = new Label("TURNO ENEMIGO");
                    lblTurno.setFont(Font.font("Minecraftia", 50));
                    paneTodo.getChildren().add(lblTurno);
                    
                    
                    
//                    siguiente.setTranslateX(200);
//                    siguiente.setTranslateY(75);
                    siguiente.setDisable(false);
                    siguiente.setOnAction(null);
                    siguiente.setOnAction(e -> turnoEnemigo());
                    
                }
                //palabras.getChildren().remove(siguiente);
                
            }
            menu.getChildren().clear();
            palabras.getChildren().clear();
            if(pokeVivosEn.isEmpty()){
                //Usuario gana
                this.ganador = usuario;
                palabraA.setText("¡Belleza mayonesa!");
                //Si gana contra lider
                if(enemigo instanceof Lider){
                    System.out.println("Entra a lider al final uwu");
                    palabraA.setText("Lider: Ya veo, ya.");
                    palabraB.setText("¡Has vencido a Lider de Gimnasio!");
                    usuario.setCampeon(true);
                    siguiente.setOnAction(null);
                    siguiente.setOnAction(e -> finJuego());
                }else{
                    palabraB.setText("Has ganado 300₽");
                    usuario.setDinero(usuario.getDinero()+300);
                }
                palabras.getChildren().addAll(palabraA,palabraB);
                menu.getChildren().add(palabras);
                siguiente.setDisable(false);
                siguiente.setOnAction(null);
                siguiente.setOnAction(e -> finPantallaMundo());
            }if(pokeVivosJug.isEmpty()){
                //Usuario pierde
                palabraA.setText("¡Tus pokemons se han debilitado!");
                palabraB.setText("¡"+usuario.getNombre()+" ha perdido!");
                palabras.getChildren().addAll(palabraA,palabraB);
                menu.getChildren().add(palabras);
                siguiente.setDisable(false);
                siguiente.setOnAction(null);
                siguiente.setOnAction(e -> finPantallaHospital());
            }
        }
               
    }
    
    
    /**
     * Organiza turno del enemigo
     */
    public void turnoEnemigo(){
        System.out.println("entra a turno enemigo");
        
        deshabilitarBotones(true);
        if(poke2.getHp()>0){
            enemigoAtaca();
        }else{
            if(!pokeVivosEn.isEmpty()){
                paneTodo.getChildren().remove(poke2);
                enemigoCambiaPoke();
            }

        }
                  
    }
    
    /**
     * Organiza turno del usuario
     */
    public void turnoUsuario(){
        paneTodo.getChildren().remove(lblTurno);
        System.out.println("turno usuario");
        organizarElementos();        
        crearEventos();
        
    }
    
    /**
     * Manejo del turno del enemigo
     */
    public void enemigoAtaca(){
        siguiente.setOnAction(null);
        menu.getChildren().clear();
        palabras.getChildren().clear();
        paneTodo.getChildren().remove(lblTurno);
        String[] ataques = poke2.getAtaques();
        java.util.Random random = new java.util.Random();
        int a = random.nextInt(ataques.length);
        String ataque = ataques[a];
        palabraA.setText(poke2.getNombre()+" usó "+ataque);
        double danio = formulaAtaque(poke2,poke1);
        double vida = poke1.getHp()-danio;
        poke1.setHp(vida);
        
        
        if(poke1.getHp()>0){
            System.out.println("entra a enemigo ataca ");
            palabraB.setText(poke1.getNombre() + " recibió "+df.format(danio)+" de daño!. HP restante: "+df.format(poke1.getHp()));
            poke1.setHp(vida);
            siguiente.setDisable(false);
            siguiente.setOnAction(e -> inicioBatalla());
            palabras.getChildren().addAll(palabraA,palabraB);
            menu.getChildren().addAll(palabras);
            
        }
        else{
            palabraB.setText("!" + poke1.getNombre()+" se ha debilitado!");
            poke1.setHp(0);
            palabras.getChildren().addAll(palabraA,palabraB);
            menu.getChildren().addAll(palabras);
            pokeVivosJug.remove(poke1);
            paneTodo.getChildren().remove(poke1Img);
            if(!pokeVivosJug.isEmpty()){
//                cambiarPokemonUsuario();
                siguiente.setDisable(false);
                siguiente.setOnAction(e ->cambiarPokemonUsuario());
                //inicioBatalla();
                
            }else{
                siguiente.setDisable(false);
                siguiente.setOnAction(e ->inicioBatalla());
            }
            
            
        }
        
        siguiente.setDisable(false);
        
        //siguiente.setOnAction(e -> inicioBatalla());
        
         
        
    }
    
    
    /**
     * Al hacer click en botón de pokemón
     */
    public void pokeEscogido(Pokemon p){
        System.out.println(cambioPoke);
        if(p.equals(poke1)){
            //Si usuario escoge el mismo pokemon que ya tenia escogido
            palabras.getChildren().clear();
            menu.getChildren().clear();
            //palabras.getChildren().add(poke);
            palabraB.setText("Ya tienes escogido a ese Pokemon");
            
            menu.getChildren().add(palabraB);
            siguiente.setDisable(false);
            siguiente.setOnAction(null);
            siguiente.setOnAction(e -> inicioBatalla());
            
        }
        else{
            //Cambia poke1 (el que usuario esta usando) a otro
            if(poke1.getHp()>0){
                cambioPoke +=1;
                System.out.println(cambioPoke);
            }
            
            paneTodo.getChildren().remove(poke1Img);
            if(turno == "Jugador"){
                siguiente.setDisable(false);
                siguiente.setOnAction(null);
                siguiente.setOnAction(e -> inicioBatalla());
            }
            this.poke1 = p;
            menu.getChildren().clear();
            palabraA.setText("¡Ve "+p.getNombre()+"!");
            organizarPokeUsuario(p);
            menu.getChildren().addAll(palabraA);
            //paneTodo.getChildren().add(siguiente);
            //organizarPokeUsuario(poke1);
            
            
        }    
    }
       
   
    /**
     * Si usuario cambia pokemon
     */
    public void cambiarPokemonUsuario(){
        menu.getChildren().clear();
        opciones.getChildren().clear();
        pokeCambiar.getChildren().clear();
        System.out.println("A cambiarr");
        VBox cambio = new VBox();
        
        if(cambioPoke <3){
            System.out.println("Llega aca uwu");
                palabraA.setText("¿A cuál desea cambiar?");

            for(Pokemon p:pokeVivosJug){
                Button pokeNombre = new Button(p.getNombre());
                pokeNombre.setFont(Font.font("Minecraftia", 17));
                pokeNombre.setOnAction(null);
                pokeNombre.setOnAction(e -> pokeEscogido(p));
                pokeCambiar.getChildren().addAll(pokeNombre);
            }
            cambio.getChildren().addAll(palabraA,pokeCambiar);
            menu.getChildren().add(cambio);
        }
        else{
            palabraA.setText("¡Solo puedes cambiar tres veces de POKéMON!");
            menu.getChildren().add(palabraA);
            siguiente.setDisable(false);
            siguiente.setOnAction(null);
            siguiente.setOnAction(e -> inicioBatalla());
        }

    }
    
    
    /**
     * Cuando enemigo cambia pokemon
     */
    public void enemigoCambiaPoke(){
        
        for(Pokemon pokeenemigo:pokeVivosEn){
            if(pokeenemigo.getHp()>0){
            poke2 = pokeenemigo;
            break;
            }
        }
        
        palabraA.setText(enemigo.getNombre()+" usa a "+ poke2.getNombre());
        organizarPokeEnemigo(poke2);
        
        palabras.getChildren().clear();
        palabras.getChildren().add(palabraA);
        siguiente.setDisable(false);
        siguiente.setOnAction(null);
        siguiente.setOnAction(e -> inicioBatalla());
        
    }
          
    
    
    /**
     * Calcula la bonificacion que debe recibir el pokemon por daño dependiendo del tipo
     * @param pokeAtaca
     * @param pokeRecibe
     * @return 
     */
    private double formulaBonificacion(Pokemon pokeAtaca, Pokemon pokeRecibe){
        double bonificacion = 0;
        
        if("grass".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_grass();  
        }
        if("water".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_water();
        }
        if("poison".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_poison();
        }
        if("fire".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_fire(); 
        }
        if("bug".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_bug();
        }
        if("flying".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_flying();
        }
        if("normal".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_normal();
        }
        if("dark".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_dark();
        }
        if("electric".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_electric();
        }
        if("ground".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_ground();
        }
        if("ice".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_ice();
        }
        if("fairy".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_fairy(); 
        }
        if("fighting".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_fight();
        }
        if("psychic".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_psychic();
        }
        if("rock".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_rock();
        }
        if("steel".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_steel();
        }
        if("ghost".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_ghost();
        }
        if("dragon".equals(pokeRecibe.getTipo1())){
            bonificacion = pokeRecibe.getAgainst_dragon();
        }

        if(!"".equals(pokeRecibe.getTipo2())){
            if("grass".equals(pokeRecibe.getTipo2())){
                bonificacion = 0.5*(bonificacion+pokeRecibe.getAgainst_grass());
            }
            if("water".equals(pokeRecibe.getTipo2())){
                bonificacion = 0.5*(bonificacion+pokeRecibe.getAgainst_water());
            }
            if("poison".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_poison()+bonificacion)*0.5;
            }
            if("fire".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_fire()+bonificacion)*0.5;
            }
            if("bug".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_bug()+bonificacion)*0.5;
            }
            if("flying".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_flying()+bonificacion)*0.5;
            }
            if("normal".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_normal()+bonificacion)*0.5;
            }
            if("dark".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_dark()+bonificacion)*0.5;
            }
            if("electric".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_electric()+bonificacion)*0.5;
            }
            if("ground".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_ground()+bonificacion)*0.5;
            }
            if("ice".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_ice()+bonificacion)*0.5;
            }
            if("fairy".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_fairy()+bonificacion)*0.5;
            }
            if("fighting".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_fight()+bonificacion)*0.5;
            }
            if("psychic".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_psychic()+bonificacion)*0.5;
            }
            if("rock".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_rock()+bonificacion)*0.5;
            }
            if("steel".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_steel()+bonificacion)*0.5;
            }
            if("ghost".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_ghost()+bonificacion)*0.5;
            }
            if("dragon".equals(pokeRecibe.getTipo2())){
                bonificacion = (pokeRecibe.getAgainst_dragon()+bonificacion)*0.5;
            }
        }
        return bonificacion;
    }
    
    /**
     * Formula para calcular el daño 
     * @param pokeAtaca
     * @param pokeRecibe
     * @return 
     */
    private double formulaAtaque(Pokemon pokeAtaca, Pokemon pokeRecibe){

        double bonificacion = formulaBonificacion(pokeAtaca,pokeRecibe);
        System.out.println(bonificacion);
        System.out.println(pokeAtaca.getAttack());
        double danio = (((0.5*pokeAtaca.getExp())*pokeAtaca.getAttack())/pokeRecibe.getDefense())*bonificacion;
        return danio;
    }
    
    
    /**
     * Deshabilita botones de pokemon, pokeball, lucha y huir
     * @param b true para deshabilitar y false para habilitar
     */
    public void deshabilitarBotones(boolean b){
        if(b){
            poke.setDisable(true);
            pokeball.setDisable(true);
            lucha.setDisable(true);
            huir.setDisable(true);
            
        }
        else{
            poke.setDisable(false);
            pokeball.setDisable(false);
            lucha.setDisable(false);
            huir.setDisable(false);
            
        }
}
    
    /**
     * Acción de botón si va a luchar
     */
    public void luchaPoke(){
        menu.getChildren().clear();
        opciones.getChildren().clear();
        pokeCambiar.getChildren().clear();
        palabraA.setText("Selecciona un ataque: ");
        VBox opc = new VBox();
        opc.getChildren().addAll(palabraA,opciones);
        menu.getChildren().addAll(opc);
        for(String ataque:poke1.getAtaques()){
            Button pokeAtaque = new Button(ataque);
            pokeAtaque.setFont(Font.font("Minecraftia", 15));
            pokeCambiar.getChildren().addAll(pokeAtaque);
            pokeAtaque.setOnAction(null);
            pokeAtaque.setOnAction(e -> seleccionarAtaque());
            
            
        }
        opciones.getChildren().add(pokeCambiar);
        
    }
    
    
    /**
     * Al ataque seleccionado le baja vida al pokemon 
     */
    private void seleccionarAtaque() {
        siguiente.setDisable(false);
        menu.getChildren().clear();
        palabras.getChildren().clear();
        System.out.println(poke1.getExp());
        //Poke1 ataca a poke2 y eso hará que poke2 pierda vida(teniendo en cuenta la defensa y el ataque)
        
        siguiente.setOnAction(null);
        double danio = formulaAtaque(poke1,poke2);
        System.out.println(danio);
        double vidaPoke2 = poke2.getHp()-danio;
        poke2.setHp(vidaPoke2);
        palabraA.setText(poke1.getNombre()+" ataca a "+poke2.getNombre() );
//        siguiente.setDisable(false);
        
        menu.getChildren().add(palabras);
        if(poke2.getHp()>0){
            palabraB.setText("¡"+poke2.getNombre()+ " recibió "+df.format(danio)+" de daño. HP restante: "+df.format(vidaPoke2));
            palabras.getChildren().addAll(palabraA,palabraB);
            siguiente.setDisable(false);
            siguiente.setOnAction(e -> inicioBatalla());
            
            
        }else{
            palabraB.setText("¡"+poke2.getNombre()+" se ha debilitado!");
            poke2.setHp(0);
            pokeVivosEn.remove(poke2);
            paneTodo.getChildren().remove(poke2Img);
            palabras.getChildren().addAll(palabraA,palabraB);
            siguiente.setDisable(false);
            if(batallaVSEntrenador() == true){
                siguiente.setOnAction(e -> enemigoCambiaPoke());
            }else{
                siguiente.setOnAction(e -> inicioBatalla());
            }
                        
        }
        
    }
    
    public Entrenador getGanador() {
        return ganador;
    }
    
    public StackPane getRoot() {
        return paneTodo;
    }
    
    
    /**
     * Metodo para atrapar pokemon
     */
    public void atraparPoke(){
        if(batallaVSEntrenador()==false){
            //VER SI HAY POKEBALL
            if(usuario.getPokeballs()>=1){
                if(usuario.getPokemonesObtenidos().size()<=5){
                    //animacionBall();
                    
                    ArrayList<Pokemon> pokeUsuario = usuario.getPokemonesObtenidos();
                    pokeUsuario.add(poke2);
                    usuario.setPokeballs(usuario.getPokeballs()-1); //Reduce número de pokeball en uno
                    usuario.setPokemonesObtenidos(pokeUsuario); //Añade pokemon a la lista del usuario
                    //Animación pokeball
                    
                    palabraB.setText("Has atrapado a: "+poke2.getNombre());
                    siguiente.setDisable(false);
                    siguiente.setOnAction(null);
                    siguiente.setOnAction(e ->finPantallaMundo());
                    
                }else{
                    palabraB.setText("¡No tienes espacio suficiente!");
                    siguiente.setDisable(false);
                    siguiente.setOnAction(null);
                    siguiente.setOnAction(e -> inicioBatalla());
                }
                
            }
            else{
                //No puede atrapar pokemon
                palabraB.setText("¡No tienes pokeballs!");
                siguiente.setDisable(false);
                siguiente.setOnAction(null);
                    siguiente.setOnAction(e -> inicioBatalla());
                
                
            }
        }
        else{
            palabraB.setText("¡No puedes atrapar un Pokemon con entrenador!");
            siguiente.setDisable(false);
            siguiente.setOnAction(null);
                    siguiente.setOnAction(e -> inicioBatalla());
        }
        
        palabras.getChildren().clear();
        menu.getChildren().clear();
        menu.getChildren().add(palabraB);
        
    }
    
    /**
     * Acción de botón si se va a huir
     */
    public void huida(){
        siguiente.setOnAction(null);
        if(batallaVSEntrenador()==true){
            //No puede huir si es contra un jugador
            palabraB.setText("¡No puedes huir!");
            palabras.getChildren().clear();
            palabras.getChildren().add(palabraB);
            siguiente.setDisable(false);
            siguiente.setOnAction(e -> inicioBatalla());
            //paneTodo.getChildren().add(siguiente);
        }else{
            palabraB.setText("¡Has huido!");
            palabras.getChildren().clear();
            menu.getChildren().clear();
            menu.getChildren().add(palabraB);
            siguiente.setDisable(false);
            siguiente.setOnAction(e -> finPantallaMundo());
            
            //Huye
        }
        
    }

    public void finPantallaMundo(){
        mediaplayer.stop();
        Mundo.crearEscena();
    }

    public void finPantallaHospital(){
        mediaplayer.stop();
        Hospital.crearEscena();
    }
    
    //CREAR VAINA
    public void finJuego(){
        mediaplayer.stop();
        
    }
   
//    
//    public void animacionBall(){
//        ImageView imgBall = new ImageView(new Image("/images/pokeball1.png"));
//        imgBall.setFitWidth(25);
//        imgBall.setFitHeight(25);
//        
////        imgBall.setLayoutX(50);
////        imgBall.setLayoutY(100);
////        
////        imgBall.setTranslateX(-50);
////        imgBall.setTranslateY(100);
//        
//        paneTodo.getChildren().add(imgBall);
//        
//        Path path = new Path();
//        ArcTo arc = new ArcTo();
//        arc.setX(-200);
//        arc.setY(-200);
//        arc.setRadiusX(100);
//        arc.setRadiusY(100);
//                
//        path.getElements().add(new MoveTo(20,20));
//        //path.getElements().add(arc);
//        path.getElements().add(new CubicCurveTo(100,200,100,120,200,120));
//        PathTransition pathTransition = new PathTransition();
//        pathTransition.setDuration(Duration.millis(1000));
//        pathTransition.setPath(path);
//        pathTransition.setNode(imgBall);
//        pathTransition.setCycleCount(1);
//        pathTransition.play();
//        
//    }
   
}
