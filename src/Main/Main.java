/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import GUIs.Formulario;
import GUIs.Gym;
import GUIs.Hospital;
import GUIs.Inicio;
import GUIs.Mundo;
import GUIs.Seleccion;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import Pokemon.Entrenador;
import Pokemon.Jugador;
import Pokemon.MundoPokemon;
import Pokemon.Pokemon;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 *
 * @author Andres
 */
public class Main extends Application{
    public static Jugador j = new Jugador();
    
    public static Stage window;
    public static MundoPokemon mp= new MundoPokemon();
    public static Inicio ini = new Inicio();
    public static Formulario form = new Formulario();
    public static Seleccion sel = new Seleccion();
    public static Mundo mun;
    public static Hospital hos = new Hospital();
    public static Gym gym = new Gym();
    public static Scene sInicio, sForm, sCarg, sSel, sMun, sHos, sGym;
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        generadorEventos();
        generadorEscenas();
        window = primaryStage;
        
        window.setScene(sInicio);
        window.setTitle("Pokemon");
        window.getIcons().add(new Image("/images/PIKAPIKA.jpg"));
        
        window.show();
                     
        
        
    }
    public static void generadorEscenas(){
        sInicio = new Scene(ini.getRoot(),600,400);
        sForm = new Scene(form.getRoot(),600,400);
        sSel = new Scene(sel.getRoot(),600,400);
        
        sGym = new Scene(gym.getRoot(),600,400);
        sHos = new Scene(hos.getRoot(),600,400);
        
    }
    
    public static void generadorEventos(){
        Button bNuevoJuego = ini.getButton();
        bNuevoJuego.setOnAction(e-> window.setScene(sForm));
        
        Button bCargarJuego = ini.getButtonCarg();
        bCargarJuego.setOnAction(e-> {cargarJuego(); Inicio.mediaplayer.stop();});
        
        Button bSalir = ini.getButtonSalir();
        bSalir.setOnAction(e-> System.exit(0));
        
        Button bCont = form.getButton();
        bCont.setOnAction(e-> window.setScene(sSel));
        
        
        //ImageView imagenJuga = mun.getImagenJugador();
        
        
        Button bIniciarJuego = sel.getButton();     
        bIniciarJuego.setOnAction(e-> {serializarJugadorInicio("jugador.ser"); Inicio.mediaplayer.stop();});
        
        Button bBulbasaur = sel.getBtnBulbasaur();
        bBulbasaur.setOnAction(e-> llenarBulbasaur());
        
        Button bCharmander = sel.getBtnCharmander();
        bCharmander.setOnAction(e-> llenarCharmander());
        
        Button bSquirtle = sel.getBtnSquirtle();
        bSquirtle.setOnAction(e-> llenarSquirtle());
        
    }
    
    
    public static void serializarJugadorInicio(String filename){
        j.setNombre(form.getNombre());
        j.setGenero(form.getGenero());
        
        System.out.println(j.getNombre()+j.getGenero());
        
        mp.setJugador(j);
        System.out.println("POKEMON ELEGIDO: "+ j.getPokeInicial());
        serializarPartida("mundoPokemon.ser");
        System.out.println("CORRECTA SERIALIZACION DE PARTIDA");
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))){
            out.writeObject(j);
            System.out.println("CORRECTA SERIALIZACION DE JUGADOR");
        }catch (IOException ex) {
            System.err.println("Error al serializar jugador"+ex);
        }
        cargarJuego();
    }
    
    public static void serializarJugador(String filename, Jugador j){
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))){
            out.writeObject(j);
        } catch (IOException ex) {
            System.err.println("Error al serializar jugador" + ex);
        }
    }
    
    //Prueba de SerializarPartida v1.0
    public static void serializarPartida(String filename){
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))){
            out.writeObject(mp);
        } catch (IOException ex) {
            System.err.println("Error al serializar mp" + ex);
        }
    }
    //Prueba de DeserializarPartida v1.0

    public static void deserializarPartida(String filename) {
        
        try (ObjectInputStream reader = new ObjectInputStream(new FileInputStream(filename))) {
            mp = (MundoPokemon) reader.readObject();
        } catch (IOException ex) {
            System.out.println("Problemas con " + ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("NO EXISTE CLASE MUNDOPOKEMON");
        }
        
    }

    public static Jugador deserializarJugador(String filename){
        Jugador juga = new Jugador();
        try (ObjectInputStream reader = new ObjectInputStream(new FileInputStream(filename))) {
            juga = (Jugador)reader.readObject();
        } catch (IOException ex) {
            System.out.println("Problemas con " + ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("NO EXISTE CLASE Jugador");
        }
        return juga;
    }
    
    public static void cargarJuego() {
        mun = new Mundo();
        sMun = new Scene(mun.getRoot(), 600, 400);

        window.setScene(sMun);
        Mundo.crearEscena();
    }
    public static void llenarBulbasaur(){
        j = (Jugador) j;
        j.setPokeInicial(mp.getPokemon("Bulbasaur"));
        Pokemon inicial = j.getPokeInicial();
        inicial.setExp(8);
    }
    public static void llenarCharmander(){
        j = (Jugador) j;
        j.setPokeInicial(mp.getPokemon("Charmander"));
        Pokemon inicial = j.getPokeInicial();
        inicial.setExp(8);
    }
    public static void llenarSquirtle(){
        j = (Jugador) j;
        j.setPokeInicial(mp.getPokemon("Squirtle"));
        Pokemon inicial = j.getPokeInicial();
        inicial.setExp(8);
    }
    
    

    public static Stage getWindow() {
        return window;
    }

    public static Scene getsHos() {
        return sHos;
    }

    public static Scene getsGym() {
        return sGym;
    }
    /**
     *
     * @return 
     */
    public static Jugador getJugador(){
        return j;
    }
    
    
    
    
}
